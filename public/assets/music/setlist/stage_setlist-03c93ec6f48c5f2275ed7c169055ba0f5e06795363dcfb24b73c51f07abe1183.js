var Setlist = React.createClass({
  displayName: 'Setlist',

  rawMarkup: function () {
    var rawMarkup = marked(this.props.children.toString(), { sanitize: true });
    return { __html: rawMarkup };
  },

  render: function () {

    var set_name = " " + this.props.artist_name;

    if (this.props.stage_name !== '' && this.props.stage_country !== '') {
      set_name = " " + this.props.artist_name + " - " + this.props.stage_name + ", " + this.props.stage_country;
    }

    return React.createElement(
      'li',
      { className: 'collection-item valign-wrapper' },
      React.createElement(
        'a',
        { href: '/music/artist-setlist/' + this.props.slug + '/' + this.props.id + '/' + this.props.setlist_slug, 'data-no-turbolink': 'true' },
        React.createElement('img', { className: 'circle', style: { marginRight: '10px' }, src: this.props.image, height: '30', width: '30' })
      ),
      React.createElement(
        'a',
        { href: '/music/artist-setlist/' + this.props.slug + '/' + this.props.id + '/' + this.props.setlist_slug, 'data-no-turbolink': 'true' },
        React.createElement(
          'div',
          { className: 'chip' },
          React.createElement(
            'i',
            { className: 'large material-icons' },
            'today'
          ),
          this.props.set_date
        ),
        set_name
      )
    );
  }
});

var SetlistCollection = React.createClass({
  displayName: 'SetlistCollection',

  render: function () {
    var setlistsNodes = this.props.data.map(function (setlist) {
      var stage_name = "";
      var stage_country = "";

      var img_src = '/images/default_artist.png';

      if (setlist.artist.image_medium !== null && setlist.artist.image_medium !== 'null') {
        img_src = setlist.artist.image_medium;
      }

      if (setlist.stage.name !== null && setlist.stage.name !== 'null') {
        stage_name = setlist.stage.name;
      }
      if (setlist.stage.country !== null && setlist.stage.country !== 'null') {
        stage_country = setlist.stage.country;
      }

      return React.createElement(Setlist, { set_date: setlist.setlist.set_date, artist_name: setlist.artist.name,
        image: img_src,
        name: setlist.setlist.name, id: setlist.setlist.id, slug: setlist.artist.slug,
        key: setlist.setlist.id, setlist_slug: setlist.setlist.slug,
        stage_name: stage_name, stage_country: stage_country });
    });
    return React.createElement(
      'ul',
      { className: 'collection' },
      setlistsNodes
    );
  }
});

var SetlistsBox = React.createClass({
  displayName: 'SetlistsBox',

  loadSetlistsFromServer: function () {
    $.ajax({
      url: this.props.url,
      dataType: 'json',
      cache: false,
      success: (function (data) {
        this.setState({ data: data });
      }).bind(this),
      error: (function (xhr, status, err) {
        console.error(this.props.url, status, err.toString());
      }).bind(this)
    });
  },
  getInitialState: function () {
    return { data: [] };
  },
  componentDidMount: function () {
    this.loadSetlistsFromServer();
    setInterval(this.loadSetlistsFromServer, this.props.pollInterval);
  },
  render: function () {

    return React.createElement(SetlistCollection, { data: this.state.data });
  }
});

var slug = document.getElementById('slug').innerHTML.trim();
var page = document.getElementById('page').innerHTML.trim();
var offset = document.getElementById('offset').innerHTML.trim();

ReactDOM.render(React.createElement(SetlistsBox, { url: "/api/stage-setlists/" + slug, pollInterval: 60000 }), document.getElementById('setlists'));
