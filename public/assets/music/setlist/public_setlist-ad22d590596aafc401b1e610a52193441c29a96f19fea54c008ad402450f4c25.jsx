var placeholder = document.createElement("li");
placeholder.className = "placeholder";

var ListCollection = React.createClass({
  render: function() {
    var tracksNodes = this.props.data.map(function(item) {
      return (
        <li className='collection-item valign-wrapper' data-id={item.id}
            key={item.id}
            draggable="true"
            onDragEnd={this.dragEnd}
            onDragStart={this.dragStart}>
          <i className="material-icons">library_music</i>  
          {item.name}
        </li>
      );
    });
    return (
      {tracksNodes}
    );
  }
});

var List = React.createClass({
  loadTracksFromServer: function() {
    $.ajax({
      url: this.props.url,
      dataType: 'json',
      cache: false,
      success: function(data) {
        this.setState({data: data.tracks});
      }.bind(this),
      error: function(xhr, status, err) {
        console.error(this.props.url, status, err.toString());
      }.bind(this)
    });
  },
  getInitialState: function() {
    return {data: []};
  },
  componentDidMount: function() {
    this.loadTracksFromServer();
  },
  render: function() {

    var btnStyle = 'waves-effect waves-light btn light-blue base ten-margin-right';
    var btnCancelStyle = 'waves-effect waves-light btn light-blue base ten-margin-right modal-trigger';
    var liStyle  = 'collection-item valign-wrapper'

    var setlistId  = document.getElementById('id').innerHTML.trim();
    var artistSlug = document.getElementById('slug').innerHTML.trim();
    var hrefBack   = "/music/setlist/set-songs/"+ artistSlug + "/" + setlistId ;

    var listItems = this.state.data.map((function(item, i) {
      return (
        <li id = {item.id} className = {liStyle} data-id = {i}
            key={i}
            draggable="true"
            onDragEnd={this.dragEnd}
            onDragStart={this.dragStart}>
          <i className="material-icons">library_music</i>   
          {" " + (i + 1) + ". " + item.name}
        </li>
      );
    }).bind(this));
    return (
      <div>
        <ul className = 'collection'>
          {listItems}
        </ul>
      </div>  
    );
  }
});

var id =  document.getElementById('id').innerHTML.trim();

ReactDOM.render(
  <List url={"/music/setlist/get-songs/" + id} />,
   document.getElementById('setlists')
);


