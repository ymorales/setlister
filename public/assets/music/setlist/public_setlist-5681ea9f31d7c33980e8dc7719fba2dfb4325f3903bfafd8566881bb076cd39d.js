var placeholder = document.createElement("li");
placeholder.className = "placeholder";

var ListCollection = React.createClass({
  displayName: "ListCollection",

  render: function () {
    var tracksNodes = this.props.data.map(function (item) {
      return React.createElement(
        "li",
        { className: "collection-item valign-wrapper", "data-id": item.id,
          key: item.id,
          draggable: "true",
          onDragEnd: this.dragEnd,
          onDragStart: this.dragStart },
        React.createElement(
          "i",
          { className: "material-icons" },
          "library_music"
        ),
        item.name
      );
    });
    return { tracksNodes: tracksNodes };
  }
});

var List = React.createClass({
  displayName: "List",

  loadTracksFromServer: function () {
    $.ajax({
      url: this.props.url,
      dataType: 'json',
      cache: false,
      success: (function (data) {
        this.setState({ data: data.tracks });
      }).bind(this),
      error: (function (xhr, status, err) {
        console.error(this.props.url, status, err.toString());
      }).bind(this)
    });
  },
  getInitialState: function () {
    return { data: [] };
  },
  componentDidMount: function () {
    this.loadTracksFromServer();
  },
  render: function () {

    var btnStyle = 'waves-effect waves-light btn light-blue base ten-margin-right';
    var btnCancelStyle = 'waves-effect waves-light btn light-blue base ten-margin-right modal-trigger';
    var liStyle = 'collection-item valign-wrapper';

    var setlistId = document.getElementById('id').innerHTML.trim();
    var artistSlug = document.getElementById('slug').innerHTML.trim();
    var hrefBack = "/music/setlist/set-songs/" + artistSlug + "/" + setlistId;

    var listItems = this.state.data.map((function (item, i) {
      return React.createElement(
        "li",
        { id: item.id, className: liStyle, "data-id": i,
          key: i,
          draggable: "true",
          onDragEnd: this.dragEnd,
          onDragStart: this.dragStart },
        React.createElement(
          "i",
          { className: "material-icons" },
          "library_music"
        ),
        " " + (i + 1) + ". " + item.name
      );
    }).bind(this));
    return React.createElement(
      "div",
      null,
      React.createElement(
        "ul",
        { className: "collection" },
        listItems
      )
    );
  }
});

var id = document.getElementById('id').innerHTML.trim();

ReactDOM.render(React.createElement(List, { url: "/music/setlist/get-songs/" + id }), document.getElementById('setlists'));
