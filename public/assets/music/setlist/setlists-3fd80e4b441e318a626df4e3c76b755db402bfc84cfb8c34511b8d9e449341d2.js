var Setlist = React.createClass({
  rawMarkup: function() {
    var rawMarkup = marked(this.props.children.toString(), {sanitize: true});
    return { __html: rawMarkup };
  },

  render: function() {
    
    return (
      <li className = "collection-item valign-wrapper">
        <a href={'/music/artist-setlist/'+ this.props.slug +'/'+ this.props.id + '/' + this.props.setlist_slug} data-no-turbolink='true'>
          <div className="chip">
            <i className="large material-icons">today</i>
            {this.props.set_date}
          </div>
          {" " + this.props.artist_name + "-" + this.props.name}
        </a>
      </li>
    );
  }
});

var SetlistCollection = React.createClass({
  render: function() {
    var setlistsNodes = this.props.data.map(function(setlist) {
      return (
        <Setlist set_date={setlist.setlist.set_date} artist_name={setlist.artist.name}
           name={setlist.setlist.name} id={setlist.setlist.id} slug={setlist.artist.slug} 
           key={setlist.setlist.id} setlist_slug={setlist.setlist.slug}>
        </Setlist>
      );
    });
    return (
      <ul className="collection">
        {setlistsNodes}
      </ul>
    );
  }
});


var SetlistsBox = React.createClass({
  loadSetlistsFromServer: function() {
    $.ajax({
      url: this.props.url,
      dataType: 'json',
      cache: false,
      success: function(data) {
        this.setState({data: data});
      }.bind(this),
      error: function(xhr, status, err) {
        console.error(this.props.url, status, err.toString());
      }.bind(this)
    });
  },
  getInitialState: function() {
    return {data: []};
  },
  componentDidMount: function() {
    this.loadSetlistsFromServer();
    setInterval(this.loadSetlistsFromServer, this.props.pollInterval);
  },
  render: function() {

    return (
      <SetlistCollection data={this.state.data} />
    );
  }
});

ReactDOM.render(
  
  <SetlistsBox url="/api/setlists" pollInterval={60000} />,
  document.getElementById('setlists')
);


