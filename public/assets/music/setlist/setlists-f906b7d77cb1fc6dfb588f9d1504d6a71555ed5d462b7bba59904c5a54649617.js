

var Setlist = React.createClass({
  displayName: 'Setlist',

  rawMarkup: function () {
    var rawMarkup = marked(this.props.children.toString(), { sanitize: true });
    return { __html: rawMarkup };
  },

  render: function () {

    return React.createElement(
      'li',
      { className: 'collection-item valign-wrapper' },
      React.createElement(
        'a',
        { href: '/music/artist-setlist/' + this.props.slug + '/' + this.props.id + '/' + this.props.setlist_slug, 'data-no-turbolink': 'true' },
        React.createElement(
          'div',
          { className: 'chip' },
          React.createElement(
            'i',
            { className: 'large material-icons' },
            'today'
          ),
          this.props.set_date
        ),
        " " + this.props.artist_name + "-" + this.props.name
      )
    );
  }
});

var SetlistCollection = React.createClass({
  displayName: 'SetlistCollection',

  render: function () {
    var setlistsNodes = this.props.data.map(function (setlist) {
      return React.createElement(Setlist, { set_date: setlist.setlist.set_date, artist_name: setlist.artist.name,
        name: setlist.setlist.name, id: setlist.setlist.id, slug: setlist.artist.slug,
        key: setlist.setlist.id, setlist_slug: setlist.setlist.slug });
    });
    return React.createElement(
      'ul',
      { className: 'collection' },
      setlistsNodes
    );
  }
});

var SetlistsBox = React.createClass({
  displayName: 'SetlistsBox',

  loadSetlistsFromServer: function () {
    $.ajax({
      url: this.props.url,
      dataType: 'json',
      cache: false,
      success: (function (data) {
        this.setState({ data: data });
      }).bind(this),
      error: (function (xhr, status, err) {
        console.error(this.props.url, status, err.toString());
      }).bind(this)
    });
  },
  getInitialState: function () {
    return { data: [] };
  },
  componentDidMount: function () {
    this.loadSetlistsFromServer();
    setInterval(this.loadSetlistsFromServer, this.props.pollInterval);
  },
  render: function () {

    return React.createElement(SetlistCollection, { data: this.state.data });
  }
});

ReactDOM.render(React.createElement(SetlistsBox, { url: '/api/setlists', pollInterval: 60000 }), document.getElementById('setlists'));
