var Festival = React.createClass({
  displayName: 'Festival',

  rawMarkup: function () {
    var rawMarkup = marked(this.props.children.toString(), { sanitize: true });
    return { __html: rawMarkup };
  },

  render: function () {

    return React.createElement(
      'div',
      { className: 'col s12 m12 l3' },
      React.createElement(
        'div',
        { className: 'card' },
        React.createElement(
          'div',
          { className: 'card-image' },
          React.createElement(
            'a',
            { href: '/music/stage/' + this.props.slug, 'data-no-turbolink': 'true' },
            React.createElement('img', { style: { minHeight: '180px', maxHeight: '180px' }, src: this.props.image })
          )
        ),
        React.createElement(
          'div',
          { className: 'card-content', style: { minHeight: '70px', maxHeight: '70px' } },
          React.createElement(
            'span',
            null,
            React.createElement(
              'a',
              { href: '/music/stage/' + this.props.slug, 'data-no-turbolink': 'true' },
              this.props.name
            )
          )
        ),
        React.createElement(
          'div',
          { className: 'card-action' },
          React.createElement(
            'a',
            { href: '', 'data-no-turbolink': 'true' },
            'View concerts'
          )
        )
      )
    );
  }
});

var FestivalsCollection = React.createClass({
  displayName: 'FestivalsCollection',

  render: function () {
    var festivalsNodes = this.props.data.map(function (festival) {

      var img_src = '/images/default_stage.png';

      return React.createElement(Festival, { id: festival.id, name: festival.name, country: festival.country, slug: festival.slug, image: img_src, key: festival.id });
    });
    return React.createElement(
      'div',
      null,
      festivalsNodes
    );
  }
});

var FestivalsBox = React.createClass({
  displayName: 'FestivalsBox',

  loadFestivalsFromServer: function () {
    //console.log(this.props.url + document.getElementById('wildcard').innerHTML);
    $.ajax({
      url: this.props.url,
      dataType: 'json',
      cache: false,
      success: (function (data) {
        this.setState({ data: data });
      }).bind(this),
      error: (function (xhr, status, err) {
        console.error(this.props.url, status, err.toString());
      }).bind(this)
    });
  },
  getInitialState: function () {
    return { data: [] };
  },
  componentDidMount: function () {
    this.loadFestivalsFromServer();
    setInterval(this.loadFestivalsFromServer, this.props.pollInterval);
  },
  render: function () {

    return React.createElement(FestivalsCollection, { data: this.state.data });
  }
});

var wildcard = document.getElementById('wildcard').innerHTML.trim();
var page = document.getElementById('page').innerHTML.trim();
var offset = document.getElementById('offset').innerHTML.trim();

ReactDOM.render(React.createElement(StagesBox, { url: "/api/search-festivals/" + wildcard + "/" + page + "/" + offset, pollInterval: 60000 }), document.getElementById('festivals'));
