var Festival = React.createClass({
  displayName: 'Festival',

  rawMarkup: function () {
    var rawMarkup = marked(this.props.children.toString(), { sanitize: true });
    return { __html: rawMarkup };
  },

  render: function () {

    return React.createElement(
      'li',
      { className: 'collection-item valign-wrapper' },
      React.createElement(
        'a',
        { href: '/music/stage/' + this.props.slug, 'data-no-turbolink': 'true' },
        React.createElement('img', { className: 'circle', style: { marginRight: '10px' }, src: this.props.image, height: '30', width: '30' })
      ),
      React.createElement(
        'a',
        { href: '/music/stage/' + this.props.slug, 'data-no-turbolink': 'true' },
        this.props.name + " - " + this.props.country
      )
    );
  }
});

var FestivalsCollection = React.createClass({
  displayName: 'FestivalsCollection',

  render: function () {
    var festivalsNodes = this.props.data.map(function (festival) {

      var img_src = '/images/default_artist.png';
      if (festival.logo.logo.url !== undefined) {
        img_src = festival.logo.logo.url;
      }

      return React.createElement(Festival, { id: festival.id, name: festival.name, country: festival.country, slug: festival.slug, image: img_src, key: festival.id });
    });
    return React.createElement(
      'ul',
      { className: 'collection' },
      festivalsNodes
    );
  }
});

var FestivalsBox = React.createClass({
  displayName: 'FestivalsBox',

  loadFestivalsFromServer: function () {
    //console.log(this.props.url + document.getElementById('wildcard').innerHTML);
    $.ajax({
      url: this.props.url,
      dataType: 'json',
      cache: false,
      success: (function (data) {
        this.setState({ data: data });
      }).bind(this),
      error: (function (xhr, status, err) {
        console.error(this.props.url, status, err.toString());
      }).bind(this)
    });
  },
  getInitialState: function () {
    return { data: [] };
  },
  componentDidMount: function () {
    this.loadFestivalsFromServer();
    setInterval(this.loadFestivalsFromServer, this.props.pollInterval);
  },
  render: function () {

    return React.createElement(FestivalsCollection, { data: this.state.data });
  }
});

ReactDOM.render(React.createElement(FestivalsBox, { url: "/api/featured-festivals", pollInterval: 60000 }), document.getElementById('festivals'));
