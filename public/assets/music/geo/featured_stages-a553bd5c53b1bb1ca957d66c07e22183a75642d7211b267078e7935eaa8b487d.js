var Stage = React.createClass({
  displayName: 'Stage',

  rawMarkup: function () {
    var rawMarkup = marked(this.props.children.toString(), { sanitize: true });
    return { __html: rawMarkup };
  },

  render: function () {

    return React.createElement(
      'div',
      { className: 'col s12 m12 l3' },
      React.createElement(
        'div',
        { className: 'card' },
        React.createElement(
          'div',
          { className: 'card-image' },
          React.createElement(
            'a',
            { href: '/music/stage/' + this.props.slug, 'data-no-turbolink': 'true' },
            React.createElement('img', { style: { minHeight: '180px', maxHeight: '180px' }, src: this.props.image })
          )
        ),
        React.createElement(
          'div',
          { className: 'card-content', style: { minHeight: '70px', maxHeight: '70px' } },
          React.createElement(
            'span',
            null,
            React.createElement(
              'a',
              { href: '/music/stage/' + this.props.slug, 'data-no-turbolink': 'true' },
              this.props.name
            )
          )
        ),
        React.createElement(
          'div',
          { className: 'card-action' },
          React.createElement(
            'a',
            { href: '', 'data-no-turbolink': 'true' },
            'View concerts'
          )
        )
      )
    );
  }
});

var StagesCollection = React.createClass({
  displayName: 'StagesCollection',

  render: function () {
    var stagesNodes = this.props.data.map(function (stage) {

      var img_src = '/images/default_stage.png';

      return React.createElement(Stage, { id: stage.id, name: stage.name, country: stage.country, slug: stage.slug, image: img_src, key: stage.id });
    });
    return React.createElement(
      'div',
      null,
      stagesNodes
    );
  }
});

var StagesBox = React.createClass({
  displayName: 'StagesBox',

  loadStagesFromServer: function () {
    //console.log(this.props.url + document.getElementById('wildcard').innerHTML);
    $.ajax({
      url: this.props.url,
      dataType: 'json',
      cache: false,
      success: (function (data) {
        this.setState({ data: data });
      }).bind(this),
      error: (function (xhr, status, err) {
        console.error(this.props.url, status, err.toString());
      }).bind(this)
    });
  },
  getInitialState: function () {
    return { data: [] };
  },
  componentDidMount: function () {
    this.loadStagesFromServer();
    setInterval(this.loadStagesFromServer, this.props.pollInterval);
  },
  render: function () {

    return React.createElement(StagesCollection, { data: this.state.data });
  }
});

ReactDOM.render(React.createElement(StagesBox, { url: "/api/featured-stages/A", pollInterval: 60000 }), document.getElementById('stages'));
