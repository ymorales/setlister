var Stage = React.createClass({
  rawMarkup: function() {
    var rawMarkup = marked(this.props.children.toString(), {sanitize: true});
    return { __html: rawMarkup };
  },

  render: function() {
    
    return (

      <li className = "collection-item valign-wrapper">
        <a href={'/music/stage/'+ this.props.slug} data-no-turbolink='true'>
          <img className='circle' style={{marginRight: '10px'}} src={this.props.image} height='30' width='30'/>
        </a>
        <a href={'/music/stage/'+ this.props.slug} data-no-turbolink='true'>
          {this.props.name + " - " + this.props.country}
        </a>
      </li>
    );
  }
});

var StagesCollection = React.createClass({
  render: function() {
    var stagesNodes = this.props.data.map(function(stage) {

      var img_src = '/images/default_artist.png';
      if (stage.image.image.url !== undefined){
        img_src = stage.image.image.url;
      }

      return (
        <Stage id={stage.id} name={stage.name} country={stage.country} slug={stage.slug} image={img_src} key={stage.id}>
        </Stage>
      );
    });
    return (
      <ul className="collection">
        {stagesNodes}
      </ul>
    );
  }
});


var StagesBox = React.createClass({
  loadStagesFromServer: function() {
    //console.log(this.props.url + document.getElementById('wildcard').innerHTML);
    $.ajax({
      url: this.props.url,
      dataType: 'json',
      cache: false,
      success: function(data) {
        this.setState({data: data});
      }.bind(this),
      error: function(xhr, status, err) {
        console.error(this.props.url, status, err.toString());
      }.bind(this)
    });
  },
  getInitialState: function() {
    return {data: []};
  },
  componentDidMount: function() {
    this.loadStagesFromServer();
    setInterval(this.loadStagesFromServer, this.props.pollInterval);
  },
  render: function() {

    return (
      <StagesCollection data={this.state.data} />
    );
  }
});

ReactDOM.render(
  
  <StagesBox url={"/api/geo/featured-stages"}  pollInterval={60000} />,
  document.getElementById('stages')
);