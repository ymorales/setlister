

var Artist = React.createClass({
  rawMarkup: function() {
    var rawMarkup = marked(this.props.children.toString(), {sanitize: true});
    return { __html: rawMarkup };
  },

  render: function() {
    
    return (
      <li className = "collection-item valign-wrapper">
        <a href={'/music/artists/'+ this.props.slug} data-no-turbolink='true'>
          <img className='circle' style={{marginRight: '10px'}} src={this.props.image} height='30' width='30'/>
        </a>
        <a href={'/music/artists/'+ this.props.slug} data-no-turbolink='true'>
          {' ' + this.props.name}
        </a>
      </li>
    );
  }
});

var FeaturedArtistsCollection = React.createClass({

  render: function() {

    var artistsNodes = this.props.data.map(function(artist) {

      var img_src = '/images/default_artist.png';
      if (artist.social_data.images !== undefined){
        img_src = artist.social_data.images[2].url;
      }
      else if(artist.artist.image_base !== undefined && artist.artist.image_base !== 'null' && artist.artist.image_high !== null){
        img_src = artist.artist.image_base;
      }
      
      return (
        <Artist id={artist.artist.id} name={artist.artist.name} image = {img_src}  slug={artist.artist.slug} key={artist.artist.id}>
        </Artist>
      );
    });
    return (
      <ul className="collection">
        {artistsNodes}
      </ul>
    );
  }
});


var FeaturedArtistsBox = React.createClass({
  loadArtistsFromServer: function() {
    $.ajax({
      url: this.props.url,
      dataType: 'json',
      cache: false,
      success: function(data) {
        this.setState({data: data});
      }.bind(this),
      error: function(xhr, status, err) {
        console.error(this.props.url, status, err.toString());
      }.bind(this)
    });
  },
  getInitialState: function() {
    return {data: []};
  },
  componentDidMount: function() {
    this.loadArtistsFromServer();
    setInterval(this.loadArtistsFromServer, this.props.pollInterval);
  },
  render: function() {

    return (
      <FeaturedArtistsCollection data={this.state.data} />
    );
  }
});

ReactDOM.renderToString(
  
  <FeaturedArtistsBox url="/api/featured_artists" pollInterval={60000} />,
  document.getElementById('featured_artists')
);