var Album = React.createClass({
  displayName: 'Album',

  rawMarkup: function () {
    var rawMarkup = marked(this.props.children.toString(), { sanitize: true });
    return { __html: rawMarkup };
  },

  render: function () {

    return React.createElement(
      'div',
      { className: 'col s12 m6 l4' },
      React.createElement(
        'div',
        { className: 'card' },
        React.createElement(
          'div',
          { className: 'card-image' },
          React.createElement('img', { style: { minHeight: '220px', maxHeight: '220px' }, src: this.props.image })
        ),
        React.createElement(
          'div',
          { className: 'card-content', style: { minHeight: '100px', maxHeight: '100px' } },
          React.createElement(
            'span',
            { style: { overflow: 'hidden', textOverflow: 'ellipsis' } },
            this.props.name
          )
        ),
        React.createElement('div', { className: 'card-action' })
      )
    );
  }
});

var AlbumsCollection = React.createClass({
  displayName: 'AlbumsCollection',

  render: function () {
    var albumNodes = this.props.data.map(function (album) {

      var img_src = '/images/default_artist.png';

      if (album.images[2] !== undefined) {
        img_src = album.images[1].url;
      }

      return React.createElement(Album, { id: album.id, name: album.name, image: img_src, key: album.id });
    });
    return React.createElement(
      'div',
      null,
      albumNodes
    );
  }
});

var AlbumsBox = React.createClass({
  displayName: 'AlbumsBox',

  loadAlbumsFromServer: function () {
    //console.log(this.props.url + document.getElementById('wildcard').innerHTML);
    $.ajax({
      url: this.props.url,
      dataType: 'json',
      cache: false,
      success: (function (data) {
        this.setState({ data: data.items });
      }).bind(this),
      error: (function (xhr, status, err) {
        console.error(this.props.url, status, err.toString());
      }).bind(this)
    });
  },
  getInitialState: function () {
    return { data: [] };
  },
  componentDidMount: function () {
    this.loadAlbumsFromServer();
    setInterval(this.loadAlbumsFromServer, this.props.pollInterval);
  },
  render: function () {

    return React.createElement(AlbumsCollection, { data: this.state.data });
  }
});

var artist_id = document.getElementById('id').innerHTML.trim();

ReactDOM.render(React.createElement(AlbumsBox, { url: "/api/artist_albums/" + artist_id, pollInterval: 60000 }), document.getElementById('albums'));
