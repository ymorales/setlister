var Artist = React.createClass({
  rawMarkup: function() {
    var rawMarkup = marked(this.props.children.toString(), {sanitize: true});
    return { __html: rawMarkup };
  },

  render: function() {
    
    return (
      <div className='col s12 m12 l3'>
        <div className='card'>
          <div className='card-image'>
            <a href={'/music/artists/'+ this.props.slug} data-no-turbolink='true'>
              <img  style={{minHeight:'180px', maxHeight:'180px' }} src={this.props.image} />
            </a>
          </div>
          <div className='card-content' style={{minHeight:'70px', maxHeight:'70px'}}>
            <span>
              <a href={'/music/artists/'+ this.props.slug} data-no-turbolink='true'>
                {this.props.name}
              </a>
            </span>
          </div>
          <div className='card-action'>
            <div className="follow-container">
              <iframe src={"https://embed.spotify.com/follow/1/?uri=spotify:artist:"+  this.props.spotify_id +"&size=basic&theme=light"} width="300" height="56" scrolling="no" frameBorder="0" style={{border:'none', overflow:'hidden'}} allowTransparency="true"></iframe>
            </div>
          </div>
          
        </div>          
      </div>
    );
  }
});

var FeaturedArtistsCollection = React.createClass({
  render: function() {
    var artistsNodes = this.props.data.map(function(artist) {

      var img_src = '/images/default_artist.png';
      if (artist.social_data.images !== undefined){
        img_src = artist.social_data.images[2].url;
      }

      return (
        <Artist id={artist.artist.id} spotify_id={artist.artist.spotify_id} slug={artist.artist.slug} name={artist.artist.name} image={img_src} key={artist.artist.id}>
        </Artist>
      );
    });
    return (
      <div>
        {artistsNodes}
      </div>
    );
  }
});


var FeaturedArtistsBox = React.createClass({
  loadArtistsFromServer: function() {
    //console.log(this.props.url + document.getElementById('wildcard').innerHTML);
    $.ajax({
      url: this.props.url,
      dataType: 'json',
      cache: false,
      success: function(data) {
        this.setState({data: data});
      }.bind(this),
      error: function(xhr, status, err) {
        console.error(this.props.url, status, err.toString());
      }.bind(this)
    });
  },
  getInitialState: function() {
    return {data: []};
  },
  componentDidMount: function() {
    this.loadArtistsFromServer();
    setInterval(this.loadArtistsFromServer, this.props.pollInterval);
  },
  render: function() {

    return (
      <FeaturedArtistsCollection data={this.state.data} />
    );
  }
});

 var page = document.getElementById('page').innerHTML.trim();

ReactDOM.render(
  
  <FeaturedArtistsBox url={"/api/featured_artists/" + page + "/20" }  pollInterval={60000} />,
  document.getElementById('artists')
);