var Album = React.createClass({
  rawMarkup: function() {
    var rawMarkup = marked(this.props.children.toString(), {sanitize: true});
    return { __html: rawMarkup };
  },

  render: function() {
    
    return (
      <div className='col s12 m6 l4'>
        <div className='card'>
          <div className='card-image'>
            <img  style={{minHeight:'220px', maxHeight:'220px' }} src={this.props.image} />
          </div>
          <div className='card-content' style={{minHeight:'100px', maxHeight:'100px'}}>
            <span style={{overflow: 'hidden', textOverflow: 'ellipsis'}}>             
              {this.props.name}
            </span>
          </div>
          <div className='card-action'>
            
          </div>
          
        </div>          
      </div>
    );
  }
});

var AlbumsCollection = React.createClass({
  render: function() {
    var albumNodes = this.props.data.map(function(album) {

      var img_src = '/images/default_artist.png';

      if (album.images[2] !== undefined){
       img_src = album.images[1].url;
      }

      return (
        <Album id={album.id} name={album.name} image = {img_src} key={album.id}>
        </Album>
      );
    });
    return (
      <div>
        {albumNodes}
      </div>
    );
  }
});


var AlbumsBox = React.createClass({
  loadAlbumsFromServer: function() {
    //console.log(this.props.url + document.getElementById('wildcard').innerHTML);
    $.ajax({
      url: this.props.url,
      dataType: 'json',
      cache: false,
      success: function(data) {
        this.setState({data: data.items});
      }.bind(this),
      error: function(xhr, status, err) {
        console.error(this.props.url, status, err.toString());
      }.bind(this)
    });
  },
  getInitialState: function() {
    return {data: []};
  },
  componentDidMount: function() {
    this.loadAlbumsFromServer();
    setInterval(this.loadAlbumsFromServer, this.props.pollInterval);
  },
  render: function() {
    //console.log("length " + Object.keys(this.state.data).length);
    if (Object.keys(this.state.data).length > 0) {
      return (
        <AlbumsCollection data={this.state.data} />
      );
    }
    else {
      return(
        <p>No data available</p>
      );
    }
  }
});

 var artist_id =  document.getElementById('id').innerHTML.trim();

ReactDOM.render(
  
  <AlbumsBox url={"/api/artist_albums/" + artist_id}  pollInterval={60000} />,
  document.getElementById('albums')
);