var Artist = React.createClass({
  displayName: 'Artist',

  rawMarkup: function () {
    var rawMarkup = marked(this.props.children.toString(), { sanitize: true });
    return { __html: rawMarkup };
  },

  render: function () {

    return React.createElement(
      'div',
      { className: 'col s12 m12 l3' },
      React.createElement(
        'div',
        { className: 'card' },
        React.createElement(
          'div',
          { className: 'card-image' },
          React.createElement(
            'a',
            { href: '/music/artists/' + this.props.slug, 'data-no-turbolink': 'true' },
            React.createElement('img', { style: { minHeight: '180px', maxHeight: '180px' }, src: this.props.image })
          )
        ),
        React.createElement(
          'div',
          { className: 'card-content', style: { minHeight: '70px', maxHeight: '70px' } },
          React.createElement(
            'span',
            null,
            React.createElement(
              'a',
              { href: '/music/artists/' + this.props.slug, 'data-no-turbolink': 'true' },
              this.props.name
            )
          )
        ),
        React.createElement(
          'div',
          { className: 'card-action' },
          React.createElement(
            'div',
            { className: 'follow-container' },
            React.createElement('iframe', { src: "https://embed.spotify.com/follow/1/?uri=spotify:artist:" + this.props.spotify_id + "&size=basic&theme=light", width: '300', height: '56', scrolling: 'no', frameBorder: '0', style: { border: 'none', overflow: 'hidden' }, allowTransparency: 'true' })
          )
        )
      )
    );
  }
});

var FeaturedArtistsCollection = React.createClass({
  displayName: 'FeaturedArtistsCollection',

  render: function () {
    var artistsNodes = this.props.data.map(function (artist) {

      var img_src = '/images/default_artist.png';
      if (artist.social_data.images !== undefined) {
        img_src = artist.social_data.images[2].url;
      } else if (artist.artist.image_base !== undefined) {
        img_src = artist.artist.image_base;
      }

      return React.createElement(Artist, { id: artist.artist.id, spotify_id: artist.artist.spotify_id, slug: artist.artist.slug, name: artist.artist.name, image: img_src, key: artist.artist.id });
    });
    return React.createElement(
      'div',
      null,
      artistsNodes
    );
  }
});

var FeaturedArtistsBox = React.createClass({
  displayName: 'FeaturedArtistsBox',

  loadArtistsFromServer: function () {
    //console.log(this.props.url + document.getElementById('wildcard').innerHTML);
    $.ajax({
      url: this.props.url,
      dataType: 'json',
      cache: false,
      success: (function (data) {
        this.setState({ data: data });
      }).bind(this),
      error: (function (xhr, status, err) {
        console.error(this.props.url, status, err.toString());
      }).bind(this)
    });
  },
  getInitialState: function () {
    return { data: [] };
  },
  componentDidMount: function () {
    this.loadArtistsFromServer();
    setInterval(this.loadArtistsFromServer, this.props.pollInterval);
  },
  render: function () {

    return React.createElement(FeaturedArtistsCollection, { data: this.state.data });
  }
});

var page = document.getElementById('page').innerHTML.trim();

ReactDOM.render(React.createElement(FeaturedArtistsBox, { url: "/api/featured_artists/" + page + "/20", pollInterval: 60000 }), document.getElementById('artists'));
