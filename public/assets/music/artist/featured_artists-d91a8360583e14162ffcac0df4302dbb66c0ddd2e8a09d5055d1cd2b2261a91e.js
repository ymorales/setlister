

var Artist = React.createClass({
  displayName: 'Artist',

  rawMarkup: function () {
    var rawMarkup = marked(this.props.children.toString(), { sanitize: true });
    return { __html: rawMarkup };
  },

  render: function () {

    return React.createElement(
      'li',
      { className: 'collection-item valign-wrapper' },
      React.createElement(
        'a',
        { href: '/music/artists/' + this.props.slug, 'data-no-turbolink': 'true' },
        React.createElement('img', { className: 'circle', style: { marginRight: '10px' }, src: this.props.image, height: '30', width: '30' })
      ),
      React.createElement(
        'a',
        { href: '/music/artists/' + this.props.slug, 'data-no-turbolink': 'true' },
        ' ' + this.props.name
      )
    );
  }
});

var FeaturedArtistsCollection = React.createClass({
  displayName: 'FeaturedArtistsCollection',

  render: function () {

    var artistsNodes = this.props.data.map(function (artist) {

      var img_src = '/images/default_artist.png';
      if (artist.social_data.images !== undefined && stage.image.url !== null && stage.image.url !== 'null') {
        img_src = artist.social_data.images[2].url;
      } else if (artist.artist.image_base !== undefined && artist.artist.image_base !== 'null' && artist.artist.image_base !== null) {
        img_src = artist.artist.image_base;
      }

      return React.createElement(Artist, { id: artist.artist.id, name: artist.artist.name, image: img_src, slug: artist.artist.slug, key: artist.artist.id });
    });
    return React.createElement(
      'ul',
      { className: 'collection' },
      artistsNodes
    );
  }
});

var FeaturedArtistsBox = React.createClass({
  displayName: 'FeaturedArtistsBox',

  loadArtistsFromServer: function () {
    $.ajax({
      url: this.props.url,
      dataType: 'json',
      cache: false,
      success: (function (data) {
        this.setState({ data: data });
      }).bind(this),
      error: (function (xhr, status, err) {
        console.error(this.props.url, status, err.toString());
      }).bind(this)
    });
  },
  getInitialState: function () {
    return { data: [] };
  },
  componentDidMount: function () {
    this.loadArtistsFromServer();
    setInterval(this.loadArtistsFromServer, this.props.pollInterval);
  },
  render: function () {

    return React.createElement(FeaturedArtistsCollection, { data: this.state.data });
  }
});

ReactDOM.render(React.createElement(FeaturedArtistsBox, { url: '/api/featured_artists', pollInterval: 60000 }), document.getElementById('featured_artists'));
