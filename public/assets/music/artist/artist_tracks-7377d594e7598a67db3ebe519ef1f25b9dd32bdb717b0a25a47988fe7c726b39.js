var Track = React.createClass({
  displayName: "Track",

  rawMarkup: function () {
    var rawMarkup = marked(this.props.children.toString(), { sanitize: true });
    return { __html: rawMarkup };
  },
  getInitialState: function () {
    return { selected: false };
  },
  handleClick: function (event) {

    if (this.state.selected == false) {
      add_to_set(this.props.id, this.props.name);
      console.log("add name " + this.props.name);
    } else {
      remove_from_set(this.props.id);
      console.log("remove name " + this.props.name);
    }

    this.setState({ selected: !this.state.selected });
  },
  render: function () {
    var text = this.state.selected ? 'selected' : ' - ';

    if (this.state.selected) {
      return React.createElement(
        "li",
        { className: "right_menu_li valign-wrapper ten-margin-bot" },
        React.createElement(
          "a",
          { style: { cursor: 'pointer' }, onClick: this.handleClick },
          React.createElement(
            "i",
            { className: "tiny material-icons blue-gray base" },
            "delete"
          ),
          this.props.name
        )
      );
    } else {
      return React.createElement(
        "li",
        { className: "right_menu_li valign-wrapper ten-margin-bot" },
        React.createElement(
          "a",
          { className: "grey_link", style: { cursor: 'pointer' }, onClick: this.handleClick },
          React.createElement(
            "i",
            { className: "tiny material-icons blue-gray base" },
            "playlist_add"
          ),
          this.props.name
        )
      );
    }
  }
});

var Album = React.createClass({
  displayName: "Album",

  rawMarkup: function () {
    var rawMarkup = marked(this.props.children.toString(), { sanitize: true });
    return { __html: rawMarkup };
  },

  render: function () {

    var trackNodes = this.props.tracks.map(function (tracks_data) {
      return React.createElement(Track, { name: tracks_data.name, id: tracks_data.id, key: tracks_data.id });
    });

    return React.createElement(
      "li",
      null,
      React.createElement(
        "div",
        { className: "collapsible-header" },
        React.createElement(
          "i",
          { className: "large material-icons" },
          "play_circle_filled"
        ),
        this.props.name + " ",
        React.createElement(
          "div",
          { className: "chip" },
          this.props.tracks.length + " tracks"
        )
      ),
      React.createElement(
        "div",
        { className: "collapsible-body" },
        React.createElement(
          "ul",
          null,
          trackNodes
        )
      )
    );
  }
});

var AlbumsCollection = React.createClass({
  displayName: "AlbumsCollection",

  render: function () {
    var albumsNodes = this.props.data.map(function (album_data) {
      return React.createElement(Album, { name: album_data.album.name, id: album_data.album.id, tracks: album_data.tracks.items, key: album_data.album.id });
    });
    return React.createElement(
      "ul",
      { className: "collapsible", "data-collapsible": "accordion" },
      albumsNodes
    );
  }
});

var AlbumsBox = React.createClass({
  displayName: "AlbumsBox",

  loadAlbumsFromServer: function () {
    $.ajax({
      url: this.props.url,
      dataType: 'json',
      cache: false,
      success: (function (data) {
        this.setState({ data: data });
      }).bind(this),
      error: (function (xhr, status, err) {
        console.error(this.props.url, status, err.toString());
      }).bind(this)
    });
  },
  getInitialState: function () {
    return { data: [] };
  },
  componentDidMount: function () {
    this.loadAlbumsFromServer();
    setInterval(this.loadAlbumsFromServer, this.props.pollInterval);
  },
  render: function () {
    console.log("WTF" + this.state.data);
    return React.createElement(AlbumsCollection, { data: this.state.data });
  }
});

var id = document.getElementById('id').innerHTML.trim();

ReactDOM.render(React.createElement(AlbumsBox, { url: "/api/artist_tracks/" + id, pollInterval: 60000 }), document.getElementById('tracks'));

$('.collapsible').collapsible({});
