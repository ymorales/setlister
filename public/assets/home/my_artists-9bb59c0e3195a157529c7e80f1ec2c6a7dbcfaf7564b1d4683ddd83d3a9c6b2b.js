

var Artist = React.createClass({
  displayName: "Artist",

  rawMarkup: function () {
    var rawMarkup = marked(this.props.children.toString(), { sanitize: true });
    return { __html: rawMarkup };
  },

  render: function () {
    var url = "/music/get-artist/" + this.props.id;
    return React.createElement(
      "li",
      { className: "right_menu_li" },
      React.createElement(
        "a",
        { href: url, "data-no-turbolink": "true" },
        this.props.name
      )
    );
  }
});

var ArtistList = React.createClass({
  displayName: "ArtistList",

  render: function () {
    var artistsNodes = this.props.data.map(function (artist) {
      return React.createElement(Artist, { name: artist.name, id: artist.id, key: artist.id });
    });
    return React.createElement(
      "ul",
      null,
      artistsNodes
    );
  }
});

var ArtistsBox = React.createClass({
  displayName: "ArtistsBox",

  loadArtistsFromServer: function () {
    $.ajax({
      url: this.props.url,
      dataType: 'json',
      cache: false,
      success: (function (data) {
        this.setState({ data: data });
      }).bind(this),
      error: (function (xhr, status, err) {
        console.error(this.props.url, status, err.toString());
      }).bind(this)
    });
  },
  getInitialState: function () {
    return { data: [] };
  },
  componentDidMount: function () {
    this.loadArtistsFromServer();
    //setInterval(this.loadArtistsFromServer, this.props.pollInterval);
  },
  render: function () {

    return React.createElement(ArtistList, { data: this.state.data });
  }
});

ReactDOM.render(React.createElement(ArtistsBox, { url: "/api/me/artists" }), document.getElementById('my_artists'));
