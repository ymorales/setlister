

var Artist = React.createClass({
  rawMarkup: function() {
    var rawMarkup = marked(this.props.children.toString(), {sanitize: true});
    return { __html: rawMarkup };
  },

  render: function() {
    var url = "/music/get-artist/" + this.props.id;
    return (
      <li className = "right_menu_li">
        <a href={url} data-no-turbolink='true'>
          {this.props.name}
        </a>
      </li>
    );
  }
});

var ArtistList = React.createClass({
  render: function() {
    var artistsNodes = this.props.data.map(function(artist) {
      return (
        <Artist name={artist.name} id={artist.id} key={artist.id}>
        </Artist>
      );
    });
    return (
      <ul>
        {artistsNodes}
      </ul>
    );
  }
});


var ArtistsBox = React.createClass({
  loadArtistsFromServer: function() {
    $.ajax({
      url: this.props.url,
      dataType: 'json',
      cache: false,
      success: function(data) {
        this.setState({data: data});
      }.bind(this),
      error: function(xhr, status, err) {
        console.error(this.props.url, status, err.toString());
      }.bind(this)
    });
  },
  getInitialState: function() {
    return {data: []};
  },
  componentDidMount: function() {
    this.loadArtistsFromServer();
    //setInterval(this.loadArtistsFromServer, this.props.pollInterval);
  },
  render: function() {

    return (
      <ArtistList data={this.state.data} />
    );
  }
});

ReactDOM.render(
  
  <ArtistsBox url="/api/me/artists" />,
  document.getElementById('my_artists')
);