

var Playlist = React.createClass({
  displayName: 'Playlist',

  rawMarkup: function () {
    var rawMarkup = marked(this.props.children.toString(), { sanitize: true });
    return { __html: rawMarkup };
  },

  render: function () {

    return React.createElement(
      'li',
      { className: 'right_menu_li' },
      React.createElement(
        'a',
        { href: '', 'data-no-turbolink': 'true' },
        this.props.name
      )
    );
  }
});

var PlaylistCollection = React.createClass({
  displayName: 'PlaylistCollection',

  render: function () {
    var playlistsNodes = this.props.data.map(function (playlist) {
      return React.createElement(Playlist, { name: playlist.name, id: playlist.id, key: playlist.id });
    });
    return React.createElement(
      'ul',
      null,
      playlistsNodes
    );
  }
});

var PlaylistsBox = React.createClass({
  displayName: 'PlaylistsBox',

  loadPlaylistsFromServer: function () {
    $.ajax({
      url: this.props.url,
      dataType: 'json',
      cache: false,
      success: (function (data) {
        this.setState({ data: data });
      }).bind(this),
      error: (function (xhr, status, err) {
        console.error(this.props.url, status, err.toString());
      }).bind(this)
    });
  },
  getInitialState: function () {
    return { data: [] };
  },
  componentDidMount: function () {
    this.loadPlaylistsFromServer();
    //setInterval(this.loadPlaylistsFromServer, this.props.pollInterval);
  },
  render: function () {

    return React.createElement(PlaylistCollection, { data: this.state.data });
  }
});

ReactDOM.render(React.createElement(PlaylistsBox, { url: '/api/me/playlists' }), document.getElementById('my_playlists'));
