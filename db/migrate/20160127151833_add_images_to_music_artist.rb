class AddImagesToMusicArtist < ActiveRecord::Migration
  def change
    add_column :music_artists, :image_ultra, :string
    add_column :music_artists, :image_high, :string
    add_column :music_artists, :image_medium, :string
    add_column :music_artists, :image_base, :string
  end
end
