class CreateJoinTableMusicArtistMusicFestival < ActiveRecord::Migration
  def change
    create_table :music_festival_artists, :id => false do |t|
      t.references :music_festival, index: true, foreign_key: true
      t.references :music_artist, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
