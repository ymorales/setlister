class AddImagesToMusicAlbum < ActiveRecord::Migration
  def change
    add_column :music_albums, :image_medium, :string
    add_column :music_albums, :image_high, :string
  end
end
