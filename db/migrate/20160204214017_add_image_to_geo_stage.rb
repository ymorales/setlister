class AddImageToGeoStage < ActiveRecord::Migration
  def change
    add_column :geo_stages, :image, :string
  end
end
