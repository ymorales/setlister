class CreateMusicTracks < ActiveRecord::Migration
  def change
    create_table :music_tracks do |t|
      t.string :name
      t.string :spotify_id
      t.string :slug

      t.timestamps null: false
    end
    add_index :music_tracks, :slug, unique: true
  end
end
