class CreateMusicAlbums < ActiveRecord::Migration
  def change
    create_table :music_albums do |t|
      t.string :name
      t.string :spotify_id
      t.string :image
      t.string :uri
      t.references :music_artist, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
