class AddGeoStageToMusicSetlist < ActiveRecord::Migration
  def change
    add_reference :music_setlists, :geo_stage, index: true, foreign_key: true
  end
end
