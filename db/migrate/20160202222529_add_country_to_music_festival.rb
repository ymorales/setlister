class AddCountryToMusicFestival < ActiveRecord::Migration
  def change
    add_column :music_festivals, :country, :string
  end
end
