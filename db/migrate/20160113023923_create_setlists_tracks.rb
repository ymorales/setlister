class CreateSetlistsTracks < ActiveRecord::Migration

  def change
    create_table :music_setlist_tracks, :id => false do |t|
      t.references :music_setlist, index: true, foreign_key: true
      t.references :music_track, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
