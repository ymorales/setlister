class AddMusicAlbumToMusicTrack < ActiveRecord::Migration
  def change
    add_reference :music_tracks, :music_album, index: true, foreign_key: true
  end
end
