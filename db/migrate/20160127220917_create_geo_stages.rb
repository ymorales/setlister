class CreateGeoStages < ActiveRecord::Migration
  def change
    create_table :geo_stages do |t|
      t.string :name
      t.string :country

      t.timestamps null: false
    end
  end
end
