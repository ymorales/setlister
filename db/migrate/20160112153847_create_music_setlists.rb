class CreateMusicSetlists < ActiveRecord::Migration
  def change
    create_table :music_setlists do |t|
      t.string :name
      t.date :set_date

      t.timestamps null: false
    end
  end
end
