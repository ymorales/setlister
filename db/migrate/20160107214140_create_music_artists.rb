class CreateMusicArtists < ActiveRecord::Migration
  def change
    create_table :music_artists do |t|
      t.string :name
      t.string :spotify_id
      t.string :spotify_uri

      t.timestamps null: false
    end
  end
end
