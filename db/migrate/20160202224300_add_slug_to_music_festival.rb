class AddSlugToMusicFestival < ActiveRecord::Migration
  def change
    add_column :music_festivals, :slug, :string
    add_index :music_festivals, :slug, unique: true
  end
end
