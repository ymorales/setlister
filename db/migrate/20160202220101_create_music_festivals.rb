class CreateMusicFestivals < ActiveRecord::Migration
  def change
    create_table :music_festivals do |t|
      t.string :name
      t.date :start_date
      t.date :end_date
      t.string :logo

      t.timestamps null: false
    end
  end
end
