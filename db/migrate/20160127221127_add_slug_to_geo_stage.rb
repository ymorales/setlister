class AddSlugToGeoStage < ActiveRecord::Migration
  def change
    add_column :geo_stages, :slug, :string
    add_index :geo_stages, :slug, unique: true
  end
end
