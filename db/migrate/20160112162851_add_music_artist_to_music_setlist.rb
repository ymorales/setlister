class AddMusicArtistToMusicSetlist < ActiveRecord::Migration
  def change
    add_reference :music_setlists, :music_artist, index: true, foreign_key: true
  end
end
