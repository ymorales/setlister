class AddOrderToMusicSetlistTrack < ActiveRecord::Migration
  def change
    add_column :music_setlist_tracks, :order, :integer
  end
end
