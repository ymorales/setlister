class AddSlugToMusicArtist < ActiveRecord::Migration
  def change
    add_column :music_artists, :slug, :string
    add_index :music_artists, :slug, unique: true
  end
end
