class AddSlugToMusicSetlist < ActiveRecord::Migration
  def change
    add_column :music_setlists, :slug, :string
    add_index :music_setlists, :slug, unique: true
  end
end
