class Music::TracksManager

  def self.get_album_tracks(access_token, album)
    @spotify_client = Spotify::Api::WebApi.new(access_token)
    @spotify_client.get_album_tracks(album[:id]).parsed_response
  end  

   def self.get_album(access_token, album)
    @spotify_client = Spotify::Api::WebApi.new(access_token)
    @spotify_client.get_artist_album(album[:id]).parsed_response
  end 

  def self.get_track(access_token, track)
    @spotify_client = Spotify::Api::WebApi.new(access_token)
    @spotify_client.get_track(track[:id]).parsed_response
  end  

end  