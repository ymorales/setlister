class Music::ArtistsManager

  def self.create_artist(access_token, artist)
    @spotify_client = Spotify::Api::WebApi.new(access_token)
    puts "THE ARTISTS"
    puts artist

    artist_data = @spotify_client.get_artist(artist[:id]).parsed_response

    new_artist = Music::Artist.find_by(spotify_id: artist[:id])

    if new_artist.nil?
      new_artist = Music::Artist.new({
            name: artist_data["name"],
            spotify_id: artist[:id],
            spotify_uri: artist_data["uri"],
            image_ultra: artist_data['images'][0].nil? ?  nil : artist_data['images'][0]['url'],
            image_high: artist_data['images'][1].nil? ?   nil : artist_data['images'][1]['url'],
            image_medium: artist_data['images'][2].nil? ? nil : artist_data['images'][2]['url'],
            image_base: artist_data['images'][3].nil? ?   nil : artist_data['images'][3]['url'],
         })

      new_artist.save!
    end
    new_artist
  end

  def self.get_artist(access_token, artist)
    @spotify_client = Spotify::Api::WebApi.new(access_token)
    @spotify_client.get_artist(artist[:id]).parsed_response
  end

  def self.artist_top_tracks(access_token, artist)
    @spotify_client = Spotify::Api::WebApi.new(access_token)
    @spotify_client.get_artist_top_tracks(artist[:id]).parsed_response
  end

  def self.artist_albums(access_token, artist)
    @spotify_client = Spotify::Api::WebApi.new(access_token)
    #puts ">>> limit #{artist[:limit]} offset #{artist[:offset]}"
    @spotify_client.get_artist_albums(artist[:id], "album", artist[:limit], artist[:offset]).parsed_response
  end

  def self.related_artists(access_token, artist)
    @spotify_client = Spotify::Api::WebApi.new(access_token)
    #puts ">>> limit #{artist[:limit]} offset #{artist[:offset]}"
    @spotify_client.get_related_artists(artist[:id]).parsed_response
  end

  def self.get_artists_by_wildcard(access_token, artist)
    @spotify_client = Spotify::Api::WebApi.new(access_token)
    @spotify_client.get_artists_by_wildcard(artist[:wildcard], artist[:limit], artist[:offset]).parsed_response
  end


end
