class Music::AlbumsManager

  def self.get_new_releases(access_token, filters)
    @spotify_client = Spotify::Api::WebApi.new(access_token)
    @spotify_client.get_releases(filters[:limit], filters[:offset]).parsed_response
  end

end
