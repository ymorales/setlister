Rails.application.routes.draw do
  devise_for :admins
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  
  devise_for :users, :controllers => { :omniauth_callbacks => "callbacks" }
  
  #authenticated :user do
  #  root :to => 'home#index', as: :authenticated_root
  #end
  root :to => 'home#index'

  get 'music/get-artist/:id' => 'music/artists#proxy_artist', as: :proxy_artist
  get 'music/artists/:slug' => 'music/artists#show', as: :show_artist
  get 'music/all-artists/:wildcard(/:page)' => 'music/artists#alphabetic_index', as: :get_alphabetic_artists
  get 'music/featured-artists(/:page)(/:offset)' => 'music/artists#featured_index', as: :get_featured_artists
  get 'music/artists/:slug/albums/:id' => 'music/artists#album_tracks', as: :show_album_tracks

  get  'music/setlists' => 'music/setlists#index', as: :all_setlists
  get  'music/setlists/:slug' => 'music/setlists#show_all', as: :artist_setlists
  get  'music/setlists/:slug/:id' => 'music/setlists#show', as: :show_setlist
  get  'music/artist-setlist/:slug/:id/:setlist_slug' => 'music/setlists#show_public', as: :show_public_setlist
  get  'music/setlist/new/:slug' => 'music/setlists#new', as: :new_setlist
  post 'music/setlist/new/:slug' => 'music/setlists#create', as: :create_setlist
  get  'music/setlist/set-songs/:slug/:id' => 'music/setlists#set_songs', as: :set_setlist
  post 'music/setlist/set-songs' => 'music/setlists#add_song', as: :add_song
  post 'music/setlist/order-songs' => 'music/setlists#order_songs', as: :order_songs
  get  'music/setlist/get-songs/:id' => 'music/setlists#get_songs', as: :get_current_songs
  post 'music/setlist/remove-song' => 'music/setlists#remove_song', as: :remove_song
  post 'music/setlist/remove-all-songs' => 'music/setlists#remove_all_songs', as: :remove_all_songs


  get  'music/stages/new' => 'geo/stage/stage#new', as: :new_stage
  post 'music/stages/new' => 'geo/stage/stage#create', as: :create_stage
  get  'music/all-stages/:wildcard(/:page)' => 'geo/stage/stage#alphabetic_index', as: :get_alphabetic_stages
  get  'music/stage/:slug(/:page)(/:offset)' => 'geo/stage/stage#show', as: :show_stage

  #festivals
  get  'music/festivals/new'    => 'music/festivals#new', as: :new_festival
  post 'music/festivals/create' => 'music/festivals#create', as: :create_festival
  get  'music/all-festivals/:wildcard(/:page)(/:offset)' => 'music/festivals#alphabetic_index', as: :get_alphabetic_festivals
  get  'music/festival/:slug'   => 'music/festivals#show', as: :show_festival
  get  'music/festival/:slug/artists'   => 'music/festivals#set_artists', as: :set_festival_artists


  #resftul
  get  'api/me/artists' => 'api/v1/me/me#my_artists', as: :api_v1_my_artists
  get  'api/me/playlists' => 'api/v1/me/me#my_playlists', as: :api_v1_my_playlists

  get  'api/setlists(/:page)(/:offset)' => 'api/v1/music/setlist/setlists#index', as: :api_v1_setlists
  get  'api/stage-setlists/:slug(/:page)(/:offset)' => 'api/v1/music/setlist/setlists#setlists_by_stage', as: :api_v1_setlists_by_stage
  get  'api/featured_artists(/:page)(/:offset)' => 'api/v1/music/artist/artists#featured_index', as: :api_v1_artists
  get  'api/alphabetic_artists/:wildcard(/:page)(/:offset)' => 'api/v1/music/artist/artists#alphabetic_index', as: :api_v1_alphabetic_artists
  get  'api/autocomplete_artists/:wildcard(/:page)(/:offset)' => 'api/v1/music/artist/artists#autocomplete', as: :api_v1_autocomplete_artists
  get  'api/artist_albums/:id(/:page)(/:offset)' => 'api/v1/music/artist/artists#artist_albums', as: :api_v1_artist_albums
  get  'api/artist_tracks/:id(/:page)(/:offset)' => 'api/v1/music/artist/artists#artists_tracks', as: :api_v1_artist_tracks


  #API stages
  get  'api/geo/search-stages/:wildcard(/:page)(/:offset)' => 'api/v1/geo/stage#search', as: :api_v1_search_stage 
  get  'api/geo/featured-stages(/:page)(/:offset)' => 'api/v1/geo/stage#featured_stages', as: :api_v1_featured_stages

  #API countries
  get  'api/util/countries' => 'api/v1/util/country#index', as: :api_v1_countries

  #API festivals
  get  'api/search-festivals/:wildcard(/:page)(/:offset)' => 'api/v1/music/festival/festivals#search', as: :api_v1_search_festival
  get  'api/featured-festivals(/:page)(/:offset)' => 'api/v1/music/festival/festivals#featured_index', as: :api_v1_featured_festivals


  #ads
  get 'ads/artist_skyscraper' => 'ads/backup_ad#artist_skyscraper', as: :artist_skyscraper_ad 

  #error pages|
    get '404', :to => "web/error#show_400", :code => 404

    get '500', :to => "web/error#show_500", :code => 500

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
