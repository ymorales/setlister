# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'

# Add additional assets to the asset load path
# Rails.application.config.assets.paths << Emoji.images_path

# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in app/assets folder are already added.
# Rails.application.config.assets.precompile += %w( search.js )
Rails.application.config.assets.precompile += %w(
        music/artist/featured_artists.js
        music/artist/top_artists.js 
        music/setlist/setlists.js 
        home/my_artists.js 
        home/my_playlists.js 
        music/setlist/order_setlist.js 
        music/artist/artist_tracks.js
        music/artist/alphabetic_artists.js
        music/geo/alphabetic_stages.js
        music/geo/featured_stages.js
        music/artist/albums.js
        music/setlist/public_setlist.js
        music/setlist/stage_setlist.js
        music/festival/alphabetic_festivals.js
        music/festival/featured_festivals.js
        jquery.easy-autocomplete.js
        )
