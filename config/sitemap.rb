# Set the host name for URL creation
SitemapGenerator::Sitemap.default_host = "http://www.setlister.me"

SitemapGenerator::Sitemap.create do
  # Put links creation logic here.
  #
  # The root path '/' and sitemap index file are added automatically for you.
  # Links are added to the Sitemap in the order they are specified.
  #
  # Usage: add(path, options={})
  #        (default options are used if you don't specify)
  #
  # Defaults: :priority => 0.5, :changefreq => 'weekly',
  #           :lastmod => Time.now, :host => default_host
  #
  # Examples:
  #
  # Add '/articles'
  #
  #   add articles_path, :priority => 0.7, :changefreq => 'daily'
  #
  # Add all articles:
  #
  #   Article.find_each do |article|
  #     add article_path(article), :lastmod => article.updated_at
  #   end

  #alphabetic artists
  ('A'..'Z').each do |wildcard|
    add get_alphabetic_artists_path(wildcard: wildcard, :priority => 0.7, :changefreq => 'always')  
  end

  #Artists details
  artists = Rails.cache.fetch("/all_artists", expires_in: 10.minutes) do
    Music::Artist.all
  end 

  artists.each do |artist|
    add show_artist_path(slug: artist.slug)
  end  

  #Alphabetic Festivals 
  ('A'..'Z').each do |wildcard|
    add get_alphabetic_festivals_path(wildcard: wildcard, :priority => 0.7, :changefreq => 'always')  
  end

  #Festivals details
  festivals  = Rails.cache.fetch("all_festivals", expires_in: 3000.minutes) do
    Music::Festival.all.order(start_date: :desc)
  end

  festivals.each do |festival|
    add show_festival_path(slug: festival.slug)
  end

  #Alphabetic stages
  ('A'..'Z').each do |wildcard|
    add get_alphabetic_stages_path(wildcard: wildcard, :priority => 0.7, :changefreq => 'always')  
  end

  #stages details
  stages  = Rails.cache.fetch("all-stages", expires_in: 3000.minutes) do
    Geo::Stage.all
  end

  stages.each do |stage|
    add show_stage_path(slug: stage.slug)
  end

end
