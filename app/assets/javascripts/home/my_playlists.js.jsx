

var Playlist = React.createClass({
  rawMarkup: function() {
    var rawMarkup = marked(this.props.children.toString(), {sanitize: true});
    return { __html: rawMarkup };
  },

  render: function() {
    
    return (
      <li className = "right_menu_li">
        <a href='' data-no-turbolink='true'>
          {this.props.name}
        </a>
      </li>
    );
  }
});

var PlaylistCollection = React.createClass({
  render: function() {
    var playlistsNodes = this.props.data.map(function(playlist) {
      return (
        <Playlist name={playlist.name} id={playlist.id} key={playlist.id}>
        </Playlist>
      );
    });
    return (
      <ul>
        {playlistsNodes}
      </ul>
    );
  }
});

var PlaylistsBox = React.createClass({
  loadPlaylistsFromServer: function() {
    $.ajax({
      url: this.props.url,
      dataType: 'json',
      cache: false,
      success: function(data) {
        this.setState({data: data});
      }.bind(this),
      error: function(xhr, status, err) {
        console.error(this.props.url, status, err.toString());
      }.bind(this)
    });
  },
  getInitialState: function() {
    return {data: []};
  },
  componentDidMount: function() {
    this.loadPlaylistsFromServer();
    //setInterval(this.loadPlaylistsFromServer, this.props.pollInterval);
  },
  render: function() {

    return (
      <PlaylistCollection data={this.state.data} />
    );
  }
});

ReactDOM.render(
  
  <PlaylistsBox url="/api/me/playlists" />,
  document.getElementById('my_playlists')
);