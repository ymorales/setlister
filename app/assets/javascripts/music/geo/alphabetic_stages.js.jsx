var Stage = React.createClass({
  rawMarkup: function() {
    var rawMarkup = marked(this.props.children.toString(), {sanitize: true});
    return { __html: rawMarkup };
  },

  render: function() {
    
    return (
      <div className='col s12 m12 l3'>
        <div className='card'>
          <div className='card-image'>
            <a href={'/music/stage/'+ this.props.slug} data-no-turbolink='true'>
              <img  style={{minHeight:'180px', maxHeight:'180px' }} src={this.props.image} />
            </a>
          </div>
          <div className='card-content' style={{minHeight:'70px', maxHeight:'70px'}}>
            <span>
              <a href={'/music/stage/'+ this.props.slug} data-no-turbolink='true'>
                {this.props.name}
              </a>
            </span>
          </div>
          <div className='card-action'>
            <a href='' data-no-turbolink='true'>
              View concerts
            </a>
          </div>
          
        </div>          
      </div>
    );
  }
});

var StagesCollection = React.createClass({
  render: function() {
    var stagesNodes = this.props.data.map(function(stage) {

      var img_src = '/images/default_stage.png';

      return (
        <Stage id={stage.id} name={stage.name} country={stage.country} slug={stage.slug} image={img_src} key={stage.id}>
        </Stage>
      );
    });
    return (
      <div>
        {stagesNodes}
      </div>
    );
  }
});


var StagesBox = React.createClass({
  loadStagesFromServer: function() {
    //console.log(this.props.url + document.getElementById('wildcard').innerHTML);
    $.ajax({
      url: this.props.url,
      dataType: 'json',
      cache: false,
      success: function(data) {
        this.setState({data: data});
      }.bind(this),
      error: function(xhr, status, err) {
        console.error(this.props.url, status, err.toString());
      }.bind(this)
    });
  },
  getInitialState: function() {
    return {data: []};
  },
  componentDidMount: function() {
    this.loadStagesFromServer();
    setInterval(this.loadStagesFromServer, this.props.pollInterval);
  },
  render: function() {

    return (
      <StagesCollection data={this.state.data} />
    );
  }
});

 var wildcard =  document.getElementById('wildcard').innerHTML.trim();
 var page = document.getElementById('page').innerHTML.trim();
 var offset   = document.getElementById('offset').innerHTML.trim();

ReactDOM.render(
  
  <StagesBox url={"/api/geo/search-stages/" + wildcard + "/" + page + "/" + offset}  pollInterval={60000} />,
  document.getElementById('stages')
);