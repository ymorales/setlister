

var placeholder = document.createElement("li");
placeholder.className = "placeholder";

var ListCollection = React.createClass({
  render: function() {
    var tracksNodes = this.props.data.map(function(item) {
      return (
        <li className='collection-item valign-wrapper' data-id={item.id}
            key={item.id}
            draggable="true"
            onDragEnd={this.dragEnd}
            onDragStart={this.dragStart}>
          <i className="material-icons">library_music</i>  
          {item.name}
        </li>
      );
    });
    return (
      {tracksNodes}
    );
  }
});

var List = React.createClass({
  loadTracksFromServer: function() {
    $.ajax({
      url: this.props.url,
      dataType: 'json',
      cache: false,
      success: function(data) {
        this.setState({data: data.tracks});
      }.bind(this),
      error: function(xhr, status, err) {
        console.error(this.props.url, status, err.toString());
      }.bind(this)
    });
  },
  getInitialState: function() {
    return {data: []};
  },
  componentDidMount: function() {
    this.loadTracksFromServer();
  },
  dragStart: function(e) {
    this.dragged = e.currentTarget;
    e.dataTransfer.effectAllowed = 'move';
    
    // Firefox requires dataTransfer data to be set
    e.dataTransfer.setData("text/html", e.currentTarget);
  },
  dragEnd: function(e) {

    this.dragged.style.display = "block";

    // Update data
    var data = this.state.data;
    var from = Number(this.dragged.dataset.id);
    var to = Number(this.over.dataset.id);
    if(from < to) to--;
    if(this.nodePlacement == "after") to++;
    data.splice(to, 0, data.splice(from, 1)[0]);
    this.setState({data: data});
  },
  dragOver: function(e) {
    e.preventDefault();
    this.dragged.style.display = "none";
    if(e.target.className == "placeholder") return;
    this.over = e.target;
    // Inside the dragOver method
    var relY = e.clientY - this.over.offsetTop;
    var height = this.over.offsetHeight / 2;
    var parent = e.target.parentNode;
    
    if(relY > height) {
      this.nodePlacement = "after";
      parent.insertBefore(placeholder, e.target.nextElementSibling);
    }
    else if(relY < height) {
      this.nodePlacement = "before"
      parent.insertBefore(placeholder, e.target);
    }
  },
  handleClick: function(event) {
    send_tracks();
  },
  handleModalClick: function(event) {
    $('#modal1').openModal();
  },
  render: function() {

    var btnStyle = 'waves-effect waves-light btn light-blue base ten-margin-right';
    var btnCancelStyle = 'waves-effect waves-light btn light-blue base ten-margin-right modal-trigger';
    var liStyle  = 'collection-item valign-wrapper'

    var setlistId  = document.getElementById('id').innerHTML.trim();
    var artistSlug = document.getElementById('slug').innerHTML.trim();
    var hrefBack   = "/music/setlist/set-songs/"+ artistSlug + "/" + setlistId ;

    var listItems = this.state.data.map((function(item, i) {
      return (
        <li id = {item.id} className = {liStyle} data-id = {i}
            key={i}
            draggable="true"
            onDragEnd={this.dragEnd}
            onDragStart={this.dragStart}>
          <i className="material-icons">library_music</i>   
          {" " + (i + 1) + ". " + item.name}
        </li>
      );
    }).bind(this));
    return (
      <div>
        <ul className = 'collection' onDragOver={this.dragOver}>
          {listItems}
        </ul>
        <a className = {btnStyle} onClick={this.handleClick} data-no-turbolink = 'true'>Save</a>
        <a href = {hrefBack} className = {btnStyle} data-no-turbolink = 'true'>Edit</a>
        <a className = {btnCancelStyle} data-no-turbolink = 'true' onClick = {this.handleModalClick}>Cancel</a>
      </div>  
    );
  }
});

var id =  document.getElementById('id').innerHTML.trim();

ReactDOM.render(
  <List url={"/music/setlist/get-songs/" + id} />,
   document.getElementById('setlists')
);

function send_tracks() {
  var setlist_id    = document.getElementById('id').innerHTML.trim(); 
  var artist_slug   = document.getElementById('slug').innerHTML.trim(); 
  var setlist_slug  = document.getElementById('setlist_slug').innerHTML.trim(); 

  var liIds = $('#setlists li').map(function(i,n) {
      return $(n).attr('id');
  }).get();

  console.log("ids " + liIds);
  var jqxhr = $.post( "/music/setlist/order-songs", {tracks: liIds, list: setlist_id}, function() {
        console.log( "success" );
        //location.reload();
        window.location.href = "/music/artist-setlist/"+artist_slug+"/"+setlist_id+"/"+setlist_slug;
    })
    .done(function() {
      console.log( "second success" );
    })
    .fail(function() {
      console.log( "error" );
    })
    .always(function() {
      console.log( "finished" );
  });
}

