var Setlist = React.createClass({
  rawMarkup: function() {
    var rawMarkup = marked(this.props.children.toString(), {sanitize: true});
    return { __html: rawMarkup };
  },

  render: function() {
    
    var set_name =  " " + this.props.artist_name;

    if(this.props.stage_name !== '' && this.props.stage_country !== '')
    {set_name =  " " + this.props.artist_name + " - " + this.props.stage_name + ", " + this.props.stage_country;}

    return (
      <li className = "collection-item valign-wrapper">
        <a href={'/music/artist-setlist/'+ this.props.slug +'/'+ this.props.id + '/' + this.props.setlist_slug} data-no-turbolink='true'>
          <img className='circle' style={{marginRight: '10px'}} src={this.props.image} height='30' width='30'/>
        </a>
        <a href={'/music/artist-setlist/'+ this.props.slug +'/'+ this.props.id + '/' + this.props.setlist_slug} data-no-turbolink='true'>
          <div className="chip">
            <i className="large material-icons">today</i>
            {this.props.set_date}
          </div>
          {set_name}
        </a>
      </li>
    );
  }
});

var SetlistCollection = React.createClass({
  render: function() {
    var setlistsNodes = this.props.data.map(function(setlist) {
    var stage_name = ""; 
    var stage_country = "";

    var img_src = '/images/default_artist.png';

    if (setlist.artist.image_medium !== null && setlist.artist.image_medium !== 'null'){
       img_src = setlist.artist.image_medium;
    }

    if(setlist.stage.name !== null && setlist.stage.name !== 'null'){stage_name = setlist.stage.name} 
    if(setlist.stage.country !== null && setlist.stage.country !== 'null'){stage_country = setlist.stage.country}   

      return (
        <Setlist set_date={setlist.setlist.set_date} artist_name={setlist.artist.name} 
           image={img_src} 
           name={setlist.setlist.name} id={setlist.setlist.id} slug={setlist.artist.slug} 
           key={setlist.setlist.id} setlist_slug={setlist.setlist.slug}
           stage_name={stage_name} stage_country={stage_country}>
        </Setlist>
      );
    });
    return (
      <ul className="collection">
        {setlistsNodes}
      </ul>
    );
  }
});


var SetlistsBox = React.createClass({
  loadSetlistsFromServer: function() {
    $.ajax({
      url: this.props.url,
      dataType: 'json',
      cache: false,
      success: function(data) {
        this.setState({data: data});
      }.bind(this),
      error: function(xhr, status, err) {
        console.error(this.props.url, status, err.toString());
      }.bind(this)
    });
  },
  getInitialState: function() {
    return {data: []};
  },
  componentDidMount: function() {
    this.loadSetlistsFromServer();
    setInterval(this.loadSetlistsFromServer, this.props.pollInterval);
  },
  render: function() {

    return (
      <SetlistCollection data={this.state.data} />
    );
  }
});


var slug   =  document.getElementById('slug').innerHTML.trim();
var page   = document.getElementById('page').innerHTML.trim();
var offset = document.getElementById('offset').innerHTML.trim();

ReactDOM.render(
  
  <SetlistsBox url={"/api/stage-setlists/" + slug + "/" + page + "/" + offset} pollInterval={60000} />,
  document.getElementById('setlists')
);
