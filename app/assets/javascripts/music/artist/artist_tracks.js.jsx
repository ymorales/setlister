var Track = React.createClass({
  rawMarkup: function() {
    var rawMarkup = marked(this.props.children.toString(), {sanitize: true});
    return { __html: rawMarkup };
  },
   getInitialState: function() {
    return {selected: false};
  },
  handleClick: function(event) {
    
    if(this.state.selected == false){
      add_to_set(this.props.id, this.props.name);
      console.log("add name " + this.props.name);
    }
    else{
      remove_from_set(this.props.id);
      console.log("remove name " + this.props.name);
    }

    this.setState({selected: (!this.state.selected)});
  },
  render: function() {
    var text = this.state.selected ? 'selected' : ' - ';

    if (this.state.selected) {
       return (
          <li className = 'right_menu_li valign-wrapper ten-margin-bot'>
            <a style={{cursor: 'pointer'}} onClick={this.handleClick}>
              <i className="tiny material-icons blue-gray base">delete</i>
              {this.props.name}  
            </a> 
          </li>
        );
    }
    else {
       return (
          <li className = 'right_menu_li valign-wrapper ten-margin-bot'>
            <a className="grey_link" style={{cursor: 'pointer'}} onClick={this.handleClick}>
              <i className="tiny material-icons blue-gray base">playlist_add</i>
              {this.props.name}
            </a>    
          </li>
        );
    }

  }
});

var Album = React.createClass({
  rawMarkup: function() {
    var rawMarkup = marked(this.props.children.toString(), {sanitize: true});
    return { __html: rawMarkup };
  },

  render: function() {

    var trackNodes = this.props.tracks.map(function(tracks_data) {
      return (
        <Track name={tracks_data.name} id={tracks_data.id}  key={tracks_data.id}>
        </Track>
      );
    });
    
    return (
      <li>
        <div className='collapsible-header'>
          <i className="large material-icons">play_circle_filled</i>
          {this.props.name + " "}
          <div className="chip">
            {this.props.tracks.length + " tracks"}
          </div>
        </div>
        <div className='collapsible-body'>
          <ul>
            {trackNodes}
          </ul>
        </div>
      </li>
    );
  }
});

var AlbumsCollection = React.createClass({
  render: function() {
    var albumsNodes = this.props.data.map(function(album_data) {
      return (
        <Album name={album_data.album.name} id={album_data.album.id} tracks={album_data.tracks.items} key={album_data.album.id}>
        </Album>
      );
    });
    return (
      <ul className = 'collapsible' data-collapsible = 'accordion'>
        {albumsNodes}
      </ul>
    );
  }
});


var AlbumsBox = React.createClass({
  loadAlbumsFromServer: function() {
    $.ajax({
      url: this.props.url,
      dataType: 'json',
      cache: false,
      success: function(data) {
        this.setState({data: data});
      }.bind(this),
      error: function(xhr, status, err) {
        console.error(this.props.url, status, err.toString());
      }.bind(this)
    });
  },
  getInitialState: function() {
    return {data: []};
  },
  componentDidMount: function() {
    this.loadAlbumsFromServer();
    setInterval(this.loadAlbumsFromServer, this.props.pollInterval);
  },
  render: function() {
    console.log("WTF" + this.state.data);
    return (
      <AlbumsCollection data={this.state.data} />
    );
  }
});



var id =  document.getElementById('id').innerHTML.trim();

ReactDOM.render(
  
  <AlbumsBox url={"/api/artist_tracks/" + id} pollInterval={60000} />,
  document.getElementById('tracks')
);

 $('.collapsible').collapsible({
       
  });