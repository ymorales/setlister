class Music::SetlistsController < ApplicationController
  before_action :set_setlists, only: [:index]
  before_action :set_tracks_data, only: [:show, :show_public, :get_songs]

  def index
    
  end  

  def show
   
    @setlist = Music::Setlist.find(params[:id])
    @artist = Music::Artist.find_by(slug: params[:slug]) 
    #@tracks = Music::Track.where(id: @tracks_data)
    @tracks = Array.new

    @tracks_data.each do |t|
      track = Rails.cache.fetch("#{t}/track_db", expires_in: 730.hours) do
        Music::Track.find(t);
      end  
      @tracks.push(track)
    end  
    #render json:{message: "success"}, status: 200
    expires_in 10.minutes, :public => true
  end  

  def show_public
    
    @setlist = Music::Setlist.find(params[:id])
    @artist = Music::Artist.find_by(slug: params[:slug]) 
    #@tracks = Music::Track.where(id: @tracks_data)
    @tracks = Array.new

    @tracks_data.each do |t|
      track = Rails.cache.fetch("#{t}/track_db", expires_in: 730.hours) do
        Music::Track.find(t);
      end  
      @tracks.push(track)
    end  
    #render json:{message: "success"}, status: 200
    expires_in 10.minutes, :public => true
  end 

  def show_all
    @artist = Music::Artist.find_by(slug: params[:slug]) 
    @setlists = Music::Setlist.where(artist: @artist).order(set_date: :desc).page(params[:page]).per(20)
    expires_in 10.minutes, :public => true
  end  

  def new
     @artist = Music::Artist.find_by(slug: params[:slug]) 
     @social_artist = Music::ArtistsManager.get_artist(@access_token, {id:@artist.spotify_id})
     @setlist = Music::Setlist.new 
  end  

  def create
    @artist = Music::Artist.find_by(slug: params[:slug]) 
    @setlist = Music::Setlist.new({name: params[:setlist_data][:name], 
                                   set_date: DateTime.strptime(params[:setlist_data][:set_date],'%d/%m/%Y'),
                                   artist: @artist})

    @stage =  Geo::Stage.where("name ILIKE ?", params[:setlist_data][:stage].split(" - ")[0]).first

    if !@stage.blank?
      @setlist.stage = @stage
    end

    if @setlist.save
      redirect_to set_setlist_path(slug: @artist.slug, id: @setlist.id)
    else  
      redirect_to show_artist_path(slug: @artist.slug)
    end
  end  

  def set_songs
    @setlist = Music::Setlist.find(params[:id])
    @artist = Music::Artist.find_by(slug: params[:slug]) 
    @social_artist = Music::ArtistsManager.get_artist(@access_token, {id:@artist.spotify_id})  
    @albums = Music::ArtistsManager.artist_albums(@access_token, {id: @artist.spotify_id, limit: 50, offset: 0})  

  end  

  def add_song
    @setlist = Music::Setlist.find(params[:list])

    Music::SetlistTrack.where({music_setlist_id: @setlist.id}).delete_all
    order = 0
    if !params[:tracks].blank?
      params[:tracks].each do |t|
        @track_data = Rails.cache.fetch("#{t}/track", expires_in: 1440.minutes) do
          Music::TracksManager.get_track(@access_token, {id: t})
        end  

        @track = Music::Track.new({name: @track_data['name'], spotify_id: @track_data['id']}) 
        if @track.save 
          setlist_track = Music::SetlistTrack.new({music_setlist_id: params[:list], music_track_id: @track.id, order: order + 1})
          setlist_track.save
          order += 1
        end  
      end 
    end   

    render json:{message: params[:tracks]}, status: 200  
  end  

  def order_songs

    Music::SetlistTrack.where({music_setlist_id: params[:list]}).delete_all
    order = 0
    if !params[:tracks].blank?
      params[:tracks].each do |t|
        setlist_track = Music::SetlistTrack.new({music_setlist_id: params[:list], music_track_id: t, order: order + 1})
          setlist_track.save
        order += 1
      end 
    end   

    render json:{message: params[:tracks]}, status: 200  
  end

  def get_songs 
    tracks = Array.new

    @tracks_data.each do |t|
      track = Rails.cache.fetch("#{t}/track_db", expires_in: 730.hours) do
        Music::Track.find(t);
      end  
      tracks.push(track)
    end  
      
    render json:{tracks: tracks}, status: 200
  end  

  def remove_song
    Music::SetlistTrack.where({music_setlist_id: params[:setlist_id], music_track_id: params[:id]}).delete_all
    render json:{message: "success"}, status: 200
  end  

  def remove_all_songs
    Music::SetlistTrack.where(music_setlist_id: params[:setlist_id]).delete_all
    Music::Setlist.where(id: params[:setlist_id]).delete_all
    render json:{message: "success"}, status: 200
  end 

  private

    def set_setlists
      @setlists = Music::Setlist.all.order(set_date: :desc).page(params[:page]).per(50)
    end 

    def set_tracks_data
      @tracks_data = Music::SetlistTrack.where(music_setlist_id: params[:id]).order(:order).pluck(:music_track_id) 
    end  

end 