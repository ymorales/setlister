class Music::ArtistsController < ApplicationController
  before_action :set_artist, only: [:show, :album_tracks]
  before_action :set_social_artist, only: [:show]
  before_action :set_tracks, only: [:show]
  before_action :set_artists, only: [:alphabetic_index]
  before_action :set_featured_artists, only: [:featured_index]
  before_action :set_artists_by_spotify_id, only: [:proxy_artist]

  def show
    @setlists = Music::Setlist.where(artist: @artist).order(set_date: :desc).page(1).per(5)

      @page = 1
      @offset = 20
      @albums = Rails.cache.fetch("#{@artist.spotify_id}/artist_albums", expires_in: 140.minutes) do
        Music::ArtistsManager.artist_albums(@access_token, {id: @artist.spotify_id, limit: @offset, offset: (@page -1) * @offset})
      end

      @artist.image_ultra   = @social_artist['images'][0].nil? ?  nil : @social_artist['images'][0]['url']
      @artist.image_high    = @social_artist['images'][1].nil? ?  nil : @social_artist['images'][1]['url']
      @artist.image_medium  = @social_artist['images'][2].nil? ?  nil : @social_artist['images'][2]['url']
      @artist.image_base    = @social_artist['images'][3].nil? ?  nil : @social_artist['images'][3]['url']
      @artist.save


    @feat_tracks = Rails.cache.fetch("#{@artist.spotify_id}/artist_featured_tracks", expires_in: 140.minutes) do
      #Music::Track.order_by_rand.where('music_album_id IN (?)', @albums_data.map{|x| x.id}).page(1).per(10)
      Music::ArtistsManager.artist_top_tracks(@access_token, {id: @artist.spotify_id})
    end

    #@related_artists = @artist.set_related_artists(@access_token)
    @related_artists = Rails.cache.fetch("#{@artist.spotify_id}/related_artists", expires_in: 140.minutes) do
      Music::ArtistsManager.related_artists(@access_token, {id: @artist.spotify_id})
    end

    expires_in 30.minutes, :public => true, 'max-stale' => 0 unless current_user
  end

  def proxy_artist

    @artist = Music::Artist.find_by(spotify_id: params[:id])

    if @artist.blank?
      @artist = Music::ArtistsManager.create_artist(@access_token, {id:  params[:id]})

      redirect_to show_artist_path(slug: @artist.slug)
    else
      redirect_to show_artist_path(slug: @artist.slug)
    end

    expires_in 1440.minutes, :public => true

  end

  def alphabetic_index

      expires_in 10.minutes, :public => true

  end

  def featured_index

    expires_in 10.minutes, :public => true
  end

  def album_tracks

    @album = Rails.cache.fetch("#{params[:id]}/album", expires_in: 140.minutes) do
      Music::TracksManager.get_album(@access_token, {id: params[:id]})
    end

    @album_tracks = Rails.cache.fetch("#{params[:id]}/albums_tracks", expires_in: 140.minutes) do
      Music::TracksManager.get_album_tracks(@access_token, {id: params[:id]})
    end

    @albums = Rails.cache.fetch("#{@artist.spotify_id}/artist_albums", expires_in: 140.minutes) do
       Music::ArtistsManager.artist_albums(@access_token, {id: @artist.spotify_id, limit: 10, offset: 0})
    end

    expires_in 30.days, :public => true
  end


  #private stuff
  private

    def set_artists_by_spotify_id
      @artist = Rails.cache.fetch("#{params[:id]}/artist", expires_in: 1440.minutes) do
        Music::Artist.find_by(spotify_id: params[:id])
      end
    end

    def set_artists
      @page = 1
      @offset = 20

      if !params[:page].nil?
        @page = params[:page].to_i
      end

      @artists = Rails.cache.fetch("#{params[:wildcard]}_#{@page}_#{@offset}/artists", expires_in: 1440.minutes) do
        Music::ArtistsManager.get_artists_by_wildcard(@access_token, {wildcard:  params[:wildcard], limit: @offset, offset: (@page -1)*@offset})
      end
    end

    def set_featured_artists
      @page = params[:page].blank? ? 1 : params[:page].to_i
      @offset = params[:offset].blank? ? 20 : params[:offset].to_i

      @featured_artists = Rails.cache.fetch("#{@page}_#{@offset}/artists", expires_in: 10.minutes) do
        ::Music::Artist.all.page(@page).per(@offset)
      end

      @all_artists = Rails.cache.fetch("all_artists", expires_in: 0.minutes) do
        ::Music::Artist.all.count
      end
    end

    def set_artist
      @artist = Rails.cache.fetch("#{params[:slug]}/artist", expires_in: 1440.minutes) do
        Music::Artist.find_by(slug: params[:slug])
      end

      if @artist.nil?
        @artist = Music::ArtistsManager.create_artist(@access_token, {id:  params[:slug]})
        @artist = Rails.cache.fetch("#{params[:slug]}/artist_by_spotifynx", expires_in: 1440.minutes) do
          Music::Artist.find_by(spotify_id: params[:slug])
        end
        puts "FIND BY"
        puts @artist.spotify_id
        if @artist.nil?
          redirect_to '/404'
        end
      end
    end

    def set_social_artist
      if !@artist.nil?
        @social_artist = Rails.cache.fetch("#{@artist.spotify_id}/social_artist", expires_in: 1440.minutes) do
          Music::ArtistsManager.get_artist(@access_token, {id:@artist.spotify_id})
        end
      end
    end

    def set_tracks
      if !@artist.nil?
        @tracks = Rails.cache.fetch("#{@artist.spotify_id}/tracks", expires_in: 1440.minutes) do
          Music::ArtistsManager.artist_top_tracks(@access_token, {id: @artist.spotify_id})
        end
      end
    end

end
