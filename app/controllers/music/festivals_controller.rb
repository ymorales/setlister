class Music::FestivalsController < ApplicationController
  #skip_before_filter :set_client, :set_user, :set_refresh_token, :set_token, :set_api_client, :set_my_artists, :set_playlists
  before_action :set_festival, only: [:show, :set_artists]

  def alphabetic_index    
    @page = params[:page].blank? ? 1 : params[:page].to_i
    @offset = params[:offset].blank? ? 10 : params[:offset].to_i

    @festivals  = Rails.cache.fetch("#{params[:wildcard]}_#{@page}_#{@offset}/festivals", expires_in: 10.minutes) do
      ::Music::Festival.search(params[:wildcard], @page, @offset)
    end

    @festivals_count =  Rails.cache.fetch("#{params[:wildcard]}/festivals_count", expires_in: 10.minutes) do
      ::Music::Festival.search_count(params[:wildcard])
    end

    expires_in 10.minutes, :public => true

  end  


  def show

  end  

  def set_artists

  end  


  def new
    @festival = Music::Festival.new 

    @countries  = Rails.cache.fetch("countries", expires_in: 730.hours) do
      ISO3166::Country.all_translated 
    end
  end  

  def create
    @festival = Music::Festival.where(name: params[:festival_data][:name].split(" - ")[0], country: params[:festival_data][:country]).first

    if @festival.blank?
      puts festival_params
      @festival = Music::Festival.new(festival_params)
      if @festival.save
        redirect_to new_festival_path 
      else
        redirect_to new_festival_path  
      end  
    else
      redirect_to new_festival_path 
    end  
  end  

  def festival_params
    params.require(:festival_data).permit(:name, :country, :logo, :logo_cache, :start_date, :end_date)
  end

  private

    def set_festival
      @festival = Rails.cache.fetch("#{params[:slug]}/festival", expires_in: 1440.minutes) do
        Music::Festival.find_by(slug: params[:slug])
      end
    end  

end  