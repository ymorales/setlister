class Api::V1::Me::MeController < ApplicationController

  def my_artists 
    render json: @my_artists, status: 200
  end

  def my_playlists
    render json: @my_playlists, status: 200
  end

end  