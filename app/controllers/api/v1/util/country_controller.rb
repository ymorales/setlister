class Api::V1::Util::CountryController < ApplicationController

  def index
    @countries  = Rails.cache.fetch("countries", expires_in: 730.hours) do
      ISO3166::Country.all_translated 
    end
    render json:{countries: @countries}, status:200
    expires_in 1440.hours, :public => true
  end  

  

end  