class Api::V1::Geo::StageController < ApplicationController
  skip_before_filter :set_client, :set_user, :set_refresh_token, :set_token, :set_api_client, :set_my_artists, :set_playlists, :set_featured_tracks  

  def search
    autcomplete_data = Array.new

    @page = params[:page].blank? ? 1 : params[:page].to_i
    @offset = params[:offset].blank? ? 10 : params[:offset].to_i

    @stages  = Rails.cache.fetch("#{params[:wildcard]}_#{@page}_#{@offset}/stages", expires_in: 10.minutes) do
      ::Geo::Stage.search(params[:wildcard], @page, @offset)
    end

    @stages.each do |stage|
      autcomplete_data.push({
        id: stage.id,
        name: "#{stage.name} - #{stage.country}",
        country: stage.country,
        slug: stage.slug 
      })
    end  

    render json: autcomplete_data, status:200
    expires_in 60.seconds, :public => true
  end 

  def featured_stages 
     @page = params[:page].blank? ? 1 : params[:page].to_i
     @offset = params[:offset].blank? ? 10 : params[:offset].to_i

     @stages  = Rails.cache.fetch("#{@page}_#{@offset}/featured-stages", expires_in: 10.minutes) do
      ::Geo::Stage.order_by_rand.all.page(@page).per(@offset)
     end

     render json: @stages, status:200
     expires_in 60.seconds, :public => true
  end                               
  

end 