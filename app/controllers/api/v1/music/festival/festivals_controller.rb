class Api::V1::Music::Festival::FestivalsController < ApplicationController

  def search
    autcomplete_data = Array.new

    @page = params[:page].blank? ? 1 : params[:page].to_i
    @offset = params[:offset].blank? ? 10 : params[:offset].to_i

    @festivals  = Rails.cache.fetch("#{params[:wildcard]}_#{@page}_#{@offset}/festivals", expires_in: 10.minutes) do
      ::Music::Festival.search(params[:wildcard], @page, @offset)
    end

    @festivals.each do |festival|
      autcomplete_data.push({
        id: festival.id,
        name: "#{festival.name} - #{festival.country}",
        logo: festival.logo,
        country: festival.country,
        slug: festival.slug 
      })
    end  

    render json: autcomplete_data, status:200
    expires_in 60.seconds, :public => true
  end 

  def featured_index

    autcomplete_data = Array.new

    @page = params[:page].blank? ? 1 : params[:page].to_i
    @offset = params[:offset].blank? ? 10 : params[:offset].to_i

    @festivals  = Rails.cache.fetch("#{@page}_#{@offset}/featured_festivals", expires_in: 30.minutes) do
      ::Music::Festival.order_by_rand.all.order(start_date: :desc).page(@page).per(@offset)
    end

    @festivals.each do |festival|
      autcomplete_data.push({
        id: festival.id,
        name: "#{festival.name} - #{festival.country}",
        logo: festival.logo,
        country: festival.country,
        slug: festival.slug 
      })
    end  

    render json: autcomplete_data, status:200
    expires_in 60.seconds, :public => true

  end  

end  