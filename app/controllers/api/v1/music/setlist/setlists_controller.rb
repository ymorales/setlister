class Api::V1::Music::Setlist::SetlistsController < ApplicationController
  skip_before_filter :set_client, :set_user, :set_refresh_token, :set_token, :set_api_client, :set_my_artists, :set_playlists

  def index
    page = params[:page].blank? ? 1 : params[:page].to_i
    offset = params[:offset].blank? ? 10 : params[:offset].to_i

    @setlists = ::Music::Setlist.all.order(set_date: :desc).page(page).per(offset)

    setlists_data = Array.new

    @setlists.each do |setlist|
      setlists_data.push({
        setlist: setlist,
        artist: setlist.artist,
        stage: setlist.stage.nil? ? nil : setlist.stage
      })
    end  

    render json: setlists_data, status: 200
    expires_in 60.seconds, :public => true
  end

  def setlists_by_stage

    @page = params[:page].blank? ? 1 : params[:page].to_i
    @offset = params[:offset].blank? ? 20 : params[:offset].to_i

    @stage = Rails.cache.fetch("#{params[:slug]}/stage", expires_in: 60.minutes) do
       ::Geo::Stage.find_by(slug: params[:slug])
    end

    @setlists = Rails.cache.fetch("#{params[:slug]}_#{@page}_#{@offset}/setlists", expires_in: 10.minutes) do
      ::Music::Setlist.where(stage: @stage).order(set_date: :desc).page(@page).per(@offset)
    end  

    setlists_data = Array.new

    @setlists.each do |setlist|
      setlists_data.push({
        setlist: setlist,
        artist: setlist.artist,
        stage: @stage
      })
    end  

    render json: setlists_data, status: 200
    expires_in 600.seconds, :public => true

  end  

  

end  