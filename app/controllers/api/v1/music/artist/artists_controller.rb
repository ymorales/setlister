class Api::V1::Music::Artist::ArtistsController < ApplicationController
  require 'uri'
 
  def featured_index
    page = params[:page].blank? ? 1 : params[:page].to_i
    offset = params[:offset].blank? ? 10 : params[:offset].to_i

    artists_data = Array.new
    artists = Rails.cache.fetch("#{page}_#{offset}/artists", expires_in: 10.minutes) do
      ::Music::Artist.order_by_rand.all.page(page).per(offset)
    end  

    artists.each do |artist|
      #social_artist =  Music::ArtistsManager.get_artist(@access_token, {id:artist.spotify_id})  
      social_artist  = Rails.cache.fetch("#{artist.spotify_id}/artist", expires_in: 1440.minutes) do
        Music::ArtistsManager.get_artist(@access_token, {id: artist.spotify_id}) 
      end  
      artists_data.push({
          artist: artist,
          social_data: social_artist
        })
    end  

    render json: artists_data, status: 200
    expires_in 3600.seconds, :public => true
  end

  def autocomplete
    @page = params[:page].blank? ? 1 : params[:page].to_i
    @offset = params[:offset].blank? ? 10 : params[:offset].to_i

    artists_data = Array.new
    artists = Rails.cache.fetch("#{params[:wildcard]}_#{@page}_#{@offset}/auto_artists", expires_in: 10.minutes) do
      #::Music::Artist.search(params[:wildcard], page, offset)
      ::Music::ArtistsManager.get_artists_by_wildcard(@access_token, {wildcard:  URI.escape(params[:wildcard]), limit: @offset, offset: (@page -1) * @offset})
    end  

    artists['artists']['items'].each do |artist| 
      artists_data.push(artist)
    end  

    render json: artists_data, status: 200
    expires_in 3600.seconds, :public => true
  end



  def alphabetic_index
    @page = params[:page].blank? ? 1 : params[:page].to_i
    @offset = params[:offset].blank? ? 20 : params[:offset].to_i
    
    @artists = Rails.cache.fetch("#{params[:wildcard]}_#{@page}_#{@offset}/artists", expires_in: 1440.minutes) do
      ::Music::ArtistsManager.get_artists_by_wildcard(@access_token, {wildcard:  params[:wildcard], limit: @offset, offset: (@page -1) * @offset})
    end

    render json: @artists, status: 200
    expires_in 1440.minutes, :public => true
  end

  def artist_albums
     @page = params[:page].blank? ? 1 : params[:page].to_i
     @offset = params[:offset].blank? ? 20 : params[:offset].to_i

     @albums = Rails.cache.fetch("#{params[:id]}/artist_albums", expires_in: 1440.minutes) do
        @artist = ::Music::Artist.find_by(spotify_id: params[:id])
        if ::Music::Album.where(artist: @artist).blank? &&  !@access_token.nil?
          ::Music::ArtistsManager.artist_albums(@access_token, {id: params[:id], limit: @offset, offset: (@page -1) * @offset})
        else
          items = Array.new
          albums = Music::Album.where(artist: @artist).page(@page).per(@offset)
          albums.each do |album_data|
            items.push({
               id: album_data.spotify_id,
               images: [{url: album_data.image_high}, {url: album_data.image_medium}, {url: album_data.image}],
               name: album_data.name
            })
          end 
          {items: items} 
        end  
     end  

     render json: @albums, status: 200
     expires_in 1440.minutes, :public => true 
  end  

  def artists_tracks
    @page = params[:page].blank? ? 1 : params[:page].to_i
    @offset = params[:offset].blank? ? 20 : params[:offset].to_i

    album_tracks = Array.new

    @albums = Rails.cache.fetch("#{params[:id]}/artist_albums", expires_in: 1440.minutes) do
        ::Music::ArtistsManager.artist_albums(@access_token, {id: params[:id], limit: @offset, offset: (@page -1) * @offset})
    end  

    @albums['items'].each do |album|
      tracks = Rails.cache.fetch("#{album['id']}/album_tracks", expires_in: 1440.minutes) do
        ::Music::TracksManager.get_album_tracks(@access_token, {id: album['id']})
      end 

      album_tracks.push({album: album, tracks: tracks})
    end   

    render json: album_tracks, status: 200
    expires_in 1440.minutes, :public => true 
  end  


end  
