class Geo::Stage::StageController < ApplicationController
  skip_before_filter :set_client, :set_user, :set_refresh_token, :set_token, :set_api_client, :set_my_artists, :set_playlists
  before_action :set_stage, only: [:show]
  before_action :set_setlists, only: [:show]

  def show

  end  


  def new
    @stage = Geo::Stage.new 

    @countries  = Rails.cache.fetch("countries", expires_in: 730.hours) do
      ISO3166::Country.all_translated 
    end
  end

  def create
    @stage =  Geo::Stage.where(name: params[:stage_data][:name].split(" - ")[0], country: params[:stage_data][:country]).first

    if @stage.blank?
=begin
      @stage = Geo::Stage.new({
         name: params[:stage_data][:name], country: params[:country]
      })
=end
       @stage = Geo::Stage.new(stage_params)

      @stage.country = params[:country]

      if @stage.save
        redirect_to get_alphabetic_stages_path(wildcard: params[:stage_data][:name][0,1].upcase)  
      else
        redirect_to new_stage_path  
      end  
    else
      redirect_to get_alphabetic_stages_path(wildcard: params[:stage_data][:name][0,1].upcase)  
    end  
  end  

  def stage_params
    params.require(:stage_data).permit(:name, :country, :image, :image_cache)
  end

  def alphabetic_index    
    @page = params[:page].blank? ? 1 : params[:page].to_i
    @offset = params[:offset].blank? ? 20 : params[:offset].to_i

    @stages  = Rails.cache.fetch("#{params[:wildcard]}_#{@page}_#{@offset}/stages", expires_in: 10.minutes) do
      ::Geo::Stage.search(params[:wildcard], @page, @offset)
    end

    @search_count =  Rails.cache.fetch("#{params[:wildcard]}/stages_count", expires_in: 10.minutes) do
      ::Geo::Stage.search_count(params[:wildcard])
    end

    expires_in 10.minutes, :public => true

  end  

  private

    def set_stage
      @stage = Rails.cache.fetch("#{params[:slug]}/stage", expires_in: 1440.minutes) do
        Geo::Stage.find_by(slug: params[:slug])
      end  
    end  

    def set_setlists
      @page = params[:page].blank? ? 1 : params[:page].to_i
      @offset = params[:offset].blank? ? 20 : params[:offset].to_i

      @setlists = Rails.cache.fetch("#{@stage.slug}_#{@page}_#{@offset}/setlists", expires_in: 10.minutes) do
        ::Music::Setlist.where(stage: @stage).order(set_date: :desc).page(@page).per(@offset)
      end  
    end  

end  