class CallbacksController < Devise::OmniauthCallbacksController

    skip_before_filter :set_token, :set_client, :set_user, :set_playlists

    def spotify
        @user = User.from_omniauth(request.env["omniauth.auth"])
        session[:omniauth] = request.env["omniauth.auth"]
        sign_in_and_redirect @user
    end
end