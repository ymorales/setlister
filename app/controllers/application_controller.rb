class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_filter :set_client, :set_user, :set_refresh_token, :set_token, :set_api_client

  def set_token
    tokenizer = Spotify::Api::Tokenizer.new
    token_response =  tokenizer.client_token
    @access_token = token_response['access_token']
    session[:access_token] =  @access_token
  end

  def set_refresh_token
    if !session[:omniauth].nil?
      @refresh_token = session[:omniauth]['credentials']['refresh_token']
    end  
  end  

  def set_client
    @client = Spotify::Client.new(access_token: session[:access_token], raise_errors: true)
  end  

  def set_user
   if !session[:omniauth].nil?
    @user = User.get_spotify_profile(session[:omniauth])
   end 
  end  

  def set_api_client
     if !session[:omniauth].nil?
      @spotify_client = Spotify::Api::WebApi.new(session[:access_token])
     end
  end  

  def set_playlists
    if !@spotify_client.nil?
      @my_playlists = Rails.cache.fetch("#{session[:omniauth]['uid']}/playlists", expires_in: 10.minutes) do
         @spotify_client.user_playlists(session[:omniauth]['uid']).parsed_response['items']
      end
      
      #expires_in 60.minutes, :public => true
    end
  end  

  def set_my_artists

    if !@spotify_client.nil? && !session[:omniauth]['uid'].blank?
      @my_artists  = Rails.cache.fetch("#{session[:omniauth]['uid']}/artists", expires_in: 10.minutes) do
        if !@spotify_client.favorite_artists().parsed_response['artists'].nil?
          @spotify_client.favorite_artists().parsed_response['artists']['items']
        end  
      end
    end

  end


  def set_featured_tracks
    @featured_tracks = Rails.cache.fetch("featured_tracks", expires_in: 10.minutes) do  
      Music::Track.order_by_rand.all.page(1).per(20)
     end 
  end  

end
