class Ads::BackupAdController < ApplicationController

  layout :layout_by_resource

  def artist_skyscraper
    @artists_data = Array.new
    artists = Rails.cache.fetch("home/artists", expires_in: 1.minutes) do
      ::Music::Artist.order_by_rand.all.page(1).per(1)
    end  

    artists.each do |artist|
      #social_artist =  Music::ArtistsManager.get_artist(@access_token, {id:artist.spotify_id})  
      social_artist  = Rails.cache.fetch("#{artist.spotify_id}/artist", expires_in: 1440.minutes) do
        Music::ArtistsManager.get_artist(@access_token, {id: artist.spotify_id}) 
      end  
      @artists_data.push({
          artist: artist,
          social_data: social_artist
        })
    end 

    @albums = Rails.cache.fetch("#{@artists_data.first[:artist][:spotify_id]}/artist_albums", expires_in: 140.minutes) do
        Music::ArtistsManager.artist_albums(@access_token, {id: @artists_data.first[:artist][:spotify_id], limit: 6, offset: 0})
    end

  end

  def layout_by_resource
    "ads_layout"
  end

end