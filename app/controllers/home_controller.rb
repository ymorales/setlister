class HomeController < ApplicationController
  #before_action :authenticate_user!, :except => [:public_index]
  before_action :set_setlists,  :only => [:index]
  before_action :set_artists,   :only => [:index]
  before_action :set_stages,    :only => [:index]
  before_action :set_festivals, :only => [:index]
  before_action :set_releases,  :only => [:index]

  def index

    expires_in 10.minutes, :public => true, 'max-stale' => 0 unless current_user
  end

  def public_index

  end

  def set_festivals

    @festivals  = Rails.cache.fetch("home/festivals", expires_in: 30.minutes) do
      ::Music::Festival.order_by_rand.all.order(start_date: :desc).page(@page).per(@offset)
    end

  end

  def set_stages
    @stages  = Rails.cache.fetch("home/stages", expires_in: 30.minutes) do
      Geo::Stage.order_by_rand.all.page(1).per(10)
    end
  end

  def set_setlists

    @setlists = Rails.cache.fetch("home/setlists", expires_in: 5.minutes) do
      Music::Setlist.all.order(set_date: :desc).page(1).per(10)
    end

    @setlists_data = Array.new

    @setlists.each do |setlist|
      @setlists_data.push({
        setlist: setlist,
        artist: setlist.artist,
        stage: setlist.stage.nil? ? nil : setlist.stage
      })
    end
  end

  def set_artists

    @artists_data = Array.new
    artists = Rails.cache.fetch("home/artists", expires_in: 10.minutes) do
      ::Music::Artist.order_by_rand.all.page(1).per(10)
    end

    artists.each do |artist|
      #social_artist =  Music::ArtistsManager.get_artist(@access_token, {id:artist.spotify_id})
      social_artist  = Rails.cache.fetch("#{artist.spotify_id}/artist", expires_in: 1440.minutes) do
        Music::ArtistsManager.get_artist(@access_token, {id: artist.spotify_id})
      end
      @artists_data.push({
          artist: artist,
          social_data: social_artist
        })
    end

  end

  def set_tracks
      @tracks = Rails.cache.fetch("#{@artist.spotify_id}/tracks", expires_in: 1440.minutes) do
        Music::ArtistsManager.artist_top_tracks(@access_token, {id: @artist.spotify_id})
      end
  end

  def set_featured_tracks
    @featured_tracks = Rails.cache.fetch("featured_tracks", expires_in: 10.minutes) do
      Music::Track.order_by_rand.all.page(1).per(20)
     end
  end

  def set_releases
    offset  = 1
    limit = 10
    @releases = Rails.cache.fetch("#{offset}_#{limit}/releases", expires_in: 1440.minutes) do
      Music::AlbumsManager.get_new_releases(@access_token, {limit: limit, offset: offset})
    end
  end

end
