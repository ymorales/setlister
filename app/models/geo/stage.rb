class Geo::Stage < ActiveRecord::Base
  extend FriendlyId
  friendly_id :name, use: :slugged

  mount_uploader :image, Music::StageUploader

  def self.search(search, page = 1 , offset = 10)
    wildcard_search = "%#{search}%"

    where("name ILIKE ?", wildcard_search)
      .page(page)
      .per(offset)
  end 

  def self.search_count(search)
    wildcard_search = "%#{search}%"

    where("name ILIKE ?", wildcard_search).count
  end 

  has_many :setlists, class_name: "Music::Setlist"   

end
