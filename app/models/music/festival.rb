class Music::Festival < ActiveRecord::Base
  extend FriendlyId
  friendly_id :name, use: :slugged

  mount_uploader :logo, Music::FestivalUploader

  def self.search(search, page = 1 , offset = 10)
    wildcard_search = "%#{search}%"

    where("name ILIKE ?", wildcard_search)
      .order(start_date: :desc)
      .page(page)
      .per(offset)
  end 

  
  def self.search_count(search)
    wildcard_search = "%#{search}%"

    where("name ILIKE ?", wildcard_search).count
  end 

end
