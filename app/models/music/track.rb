class Music::Track < ActiveRecord::Base
  extend FriendlyId
  friendly_id :name, use: :slugged

  belongs_to :album, class_name: "Music::Album", foreign_key: "music_album_id"
end
