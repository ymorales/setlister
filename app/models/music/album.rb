class Music::Album < ActiveRecord::Base
  belongs_to :artist, class_name: "Music::Artist", foreign_key: "music_artist_id"

  has_many :tracks, class_name: "Music::Track", foreign_key: "music_album_id"

end
