class Music::Artist < ActiveRecord::Base
  extend FriendlyId
  friendly_id :name, use: :slugged

  has_many :setlists, class_name: "Music::Setlist"

  def self.search(search, page = 1 , offset = 10)
    wildcard_search = "%#{search}%"

    where("name ILIKE ?", wildcard_search)
      .page(page)
      .per(offset)
  end 


  #get the related artists
  def set_related_artists(access_token)
    related_artists = Rails.cache.fetch("#{self.spotify_id}/related_artists", expires_in: 140.minutes) do
      Music::ArtistsManager.related_artists(access_token, {id: self.spotify_id})
    end 

    if !related_artists['artists'].blank?
      related_artists['artists'].each do |rel_data|
        rel_artist = Music::Artist.find_by(spotify_id: rel_data['id'])
        if  rel_artist.blank?
            rel_artist = Music::ArtistsManager.create_artist(access_token, {id: rel_data['id']})
        end
      end
    end 

    related_artists 
  end 

  #get the artists albums
  def set_artists_albums(access_token)
    @albums = Rails.cache.fetch("#{self.spotify_id}/artist_albums", expires_in: 140.minutes) do
        Music::ArtistsManager.artist_albums(access_token, {id: self.spotify_id, limit: 50, offset: 0})
    end  

    if !@albums['items'].blank?
      @albums['items'].each do |album|
        new_album = Music::Album.find_by(spotify_id: album['id'])
        if new_album.blank?
          new_album = Music::Album.new(
            name: album['name'],
            spotify_id: album['id'],
            image: album['images'].blank? ? nil : album['images'][2]['url'],
            image_medium: album['images'].blank? ? nil : album['images'][1]['url'],
            image_high: album['images'].blank? ? nil : album['images'][0]['url'],
            uri: album['uri'],
            artist: self
          )
          new_album.save
        end

          tracks = Music::TracksManager.get_album_tracks(access_token, {id: new_album.spotify_id})
          tracks['items'].each do |track_data|
            track = Music::Track.where(spotify_id: track_data['id'], album: new_album).first
            if track.blank? 
              track = Music::Track.new({name: track_data['name'], spotify_id: track_data['id'], album: new_album}) 
              track.save
            end  
          end   
      end 
    end  
  end  

end
