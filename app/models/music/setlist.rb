class Music::Setlist < ActiveRecord::Base
  extend FriendlyId
  friendly_id :name, use: :slugged

  belongs_to :artist, class_name: "Music::Artist", foreign_key: "music_artist_id"
  belongs_to :stage,  class_name: "Geo::Stage", foreign_key: "geo_stage_id"
end
