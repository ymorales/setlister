class Spotify::Api::WebApi
  include HTTParty
  base_uri ENV['spotify_api_endpoint']

  def initialize(access_token)
    @auth_bearer = { "Authorization" => "Bearer #{access_token}"}
  end

  def me
    self.class.get("/me", headers: @auth_bearer)
  end

  def favorite_artists(limit = 20, after = '')
    if after == ''
      self.class.get("/me/following?type=artist&limit=#{limit}", headers: @auth_bearer)
    else
      self.class.get("/me/following?type=artist&limit=#{limit}", headers: @auth_bearer)
    end
  end

  def user_playlists(user_id, limit = 20, offset = 0)
    self.class.get("/users/#{user_id}/playlists?offset=#{offset}&limit=#{limit}", headers: @auth_bearer)
  end

  def get_artist(id)
    self.class.get("/artists/#{id}", headers: @auth_bearer)
  end

  def get_artist_top_tracks(id)
    self.class.get("/artists/#{id}/top-tracks?country=US", headers: @auth_bearer)
  end

  def get_related_artists(id)
    self.class.get("/artists/#{id}/related-artists", headers: @auth_bearer)
  end

  def get_artist_albums(id, type = "album", limit = 20, offset = 0)
    puts "limit #{limit} offset #{offset}"
    self.class.get("/artists/#{id}/albums?type=#{type}&offset=#{offset}&limit=#{limit}", headers: @auth_bearer)
  end

  def get_artist_album(id)
    self.class.get("/albums/#{id}", headers: @auth_bearer)
  end

  def get_artists_by_wildcard(wildcard, limit = 20, offset = 0)
    self.class.get("/search?q=#{wildcard}*&type=artist&offset=#{offset}&limit=#{limit}", headers: @auth_bearer)
  end

  def get_album_tracks(id, limit = 50, offset = 0)
    self.class.get("/albums/#{id}/tracks?offset=#{offset}&limit=#{limit}", headers: @auth_bearer)
  end

  def get_track(id)
    self.class.get("/tracks/#{id}", headers: @auth_bearer)
  end

  def get_releases(limit = 10, offset = 0)
    self.class.get("/browse/new-releases", headers: @auth_bearer)
  end

end
