class Spotify::Api::Tokenizer
  include HTTParty
  base_uri ENV['spotify_token_endpoint']

  def initialize()
    authorization_credentials = Base64.strict_encode64("#{ENV['spotify_client_id']}:#{ENV['spotify_client_secret']}")
    @auth = { "Authorization" => "Basic #{authorization_credentials}"}
  end

  def new_token(refresh_token)
    puts @auth
    self.class.post("/token", body:{grant_type: "refresh_token", refresh_token: refresh_token}, headers: @auth)
  end

  def client_token()
    @parsed_response = Rails.cache.fetch("client_credentials", expires_in: 3600.seconds) do
      self.class.post("/token", body:{grant_type: "client_credentials"}, headers: @auth).parsed_response
    end
    @parsed_response
  end  

end  